﻿module Server

type Activity = {
    name: string
    state: string
    dependencies: int list
    modified: string
    created: string
    resources: int list
    }
open System.Runtime.Serialization.Json
let serialier = new DataContractJsonSerializer (typeof<Activity>)

let (|Prefix|_|) (p:string) (s:string) = 
    if s.StartsWith p then 
        Some(s.Substring (p.Length))
    else 
        None
let main args =
    let port = 8080
    use hl = new System.Net.HttpListener ()
    hl.Prefixes.Add <| sprintf "http://+:%d/" port
    hl.Start ()
    let rec loop (store : Map<string, Activity>) = 
        let ctx = hl.GetContext ()
        let req = ctx.Request
        let meth = req.HttpMethod
        let path = req.RawUrl
        let response = ctx.Response

        let reply answer status reason store' =
            response.StatusCode <- status
            response.StatusDescription <- reason
            let buffer = System.Text.Encoding.UTF8.GetBytes (answer : string)
            response.ContentLength64 <- int64 buffer.Length
            response.OutputStream.Write (buffer, 0, buffer.Length)
            response.OutputStream.Close ()
            loop store'
        match meth,path with
        | "GET", Prefix "/activities" r ->
            match Map.tryFind r store with
            | None -> reply "Nope" 404 "No" store
            | Some v -> reply (v.name) 200 "OK" store
        | "POST", Prefix "/activities" r ->
            match Map.tryFind r store with
            | None -> reply "nop" 418 "teapot" store
            | Some v -> reply "nop" 418 "teapot" store
    loop Map.empty
    0
    