# Distributed Workflow System

## AWS endpoints

- Client on AWS S3: http://bndn-workflow.s3-website.eu-central-1.amazonaws.com
- Services on AWS EC2: http://ec2-52-28-69-195.eu-central-1.compute.amazonaws.com

## Service ports

- Storage: [8000](http://ec2-52-28-69-195.eu-central-1.compute.amazonaws.com:8000/)
- Authorization: [8001](http://ec2-52-28-69-195.eu-central-1.compute.amazonaws.com:8001/)
- Deployer: [8002](http://ec2-52-28-69-195.eu-central-1.compute.amazonaws.com:8002/)
- Reserved for additional core services: 8003-8009
- Host: [8010](http://ec2-52-28-69-195.eu-central-1.compute.amazonaws.com:8010/)
- Host: [8011](http://ec2-52-28-69-195.eu-central-1.compute.amazonaws.com:8011/)
- Host: [8012](http://ec2-52-28-69-195.eu-central-1.compute.amazonaws.com:8012/)
- Reserved for additional hosts: 8013-8050

## Part 2 in your face

### Krav

- Distribueret
- Access control
- Concurrency
- Flere wfs samtidig, forskellige wfs
- Resourcer
- Deployed på server
- Virke med workflow fra GBI (kommer på DCRGraphs.net)
- Feedback fra Eduardo og GBI
- Dokumentation af hvordan systemet virker
- Brugervejledning

### Valgfri

- Sundhed af graf
- Evt. dynamisk opbygning af workflows
