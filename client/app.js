App = function(deployerURL, authURL, storageURL) {
    this.deployerURL = deployerURL;
    this.authURL = authURL;
    this.storageURL = storageURL;
    this.currentWorkflow = [2];
    this.currentRole = null;
    this.currentEvents = null;
    this.token = null;
    this.username = null;
    this.roles = null;
};

// Creates a DOM element of the specified type, html and class
App.prototype.el = function(type, html, cls) {
    var el = document.createElement(type);
    if (html) el.innerHTML = html;
    if (cls) el.className = cls;
    return el;
};

// Makes an asynchronous xhr request and executes the callback
App.prototype.http = function(method, url, body, callback, errorCB, header) {
    var xhr = new XMLHttpRequest();
    xhr.addEventListener("load", function(e) {
        if(xhr.status !== 200){
            console.log("Status code: " + xhr.status);
            errorCB(xhr.statusText);
            return;
        }
        var response;
        try {
            response = JSON.parse(xhr.responseText);
        } catch(err) {
            console.log(err);
        }
        callback(response);
    }.bind(this), false);
    xhr.open(method, url, true);
    if(header !== undefined){
        xhr.setRequestHeader(header[0], header[1]);
    }
    xhr.send(body);
};

// Batches http get requests and executes the callback
App.prototype.batchGet = function(ID, callback) {
    var cb = function(response) {
        var urls = response.events;
        var count = urls.length;
        var responses = [];
        var helper = function(i) {
            this.http("GET", urls[i], null, function(response) {
                count--;
                responses[i] = response;
                if (count <= 0) callback(responses);
            }.bind(this));
        }.bind(this);
        for (var i = 0; i < urls.length; i++) helper(i);
    };
    this.http("GET", this.deployerURL + "/" + ID, null, cb.bind(this));
};

// Add a generic select
App.prototype.addSelect = function(divName, values, choices){
    if(!choices) {
        choices = values;
    }
    var select = document.getElementById(divName);
    select.innerHTML = "";
    for(i=0; i<choices.length; i=i+1) {
        var option = this.el("option", choices[i]);
        option.value = values[i];
        select.appendChild(option);
    }
};

// Set error message
App.prototype.setErrorMessage = function(divName, message){
    var text = document.getElementById(divName);
    text.innerHTML = message;
};

// Attempts to login
App.prototype.login = function(username, password){
    var url = this.authURL + "/users/" + username + "/token";
    var body = password;
    var header = ["Password", password];
    var callback = function(response) {
        this.token = response;
        this.username = username;
        this.loadDcrs();
        this.setErrorMessage("loginError", "");
        document.getElementById("login").classList.add("hidden");
        document.getElementById("logout").classList.remove("hidden");
        document.getElementById("sidebar").classList.remove("hidden");
    };
    var errorCB = function(response) {
        this.setErrorMessage("loginError", "Incorrect username or password.");
    };
    this.http("GET", url, body, callback.bind(this), errorCB.bind(this), header);
};

// Logout
App.prototype.logout = function() {
    this.token = null;
    document.getElementById("login").classList.remove("hidden");
    document.getElementById("logout").classList.add("hidden");
    document.getElementById("main").classList.add("hidden");
    document.getElementById("sidebar").classList.add("hidden");
    document.getElementById("username").value = "";
    document.getElementById("password").value = "";
};

// Loads dcrs from storage
App.prototype.loadDcrs = function() {
    var callback = function(response) {
        if(!response || response.length === 0) {
            document.getElementById("dcrAdd").classList.add("hidden");
            return;
        }
        var names = [];
        var ids = [];
        for (var i = 0; i < response.length; i++) {
            names.push(response[i].Title);
            ids.push(response[i].ID);
        }
        this.addSelect("dcrGraphs", ids, names);
        document.getElementById("dcrAdd").classList.remove("hidden");    
    };
    this.http("GET", this.storageURL + "/dcrs/", null, callback.bind(this));
};

// Create workflow from selected DCR
App.prototype.createWorkflow = function(name, id) {
    var body = JSON.stringify({Title: name, DCRID: parseInt(id)});
    var callback = function(response) {
        console.log(response);
        this.loadWorkflow(response.title, response.id, false);
    };
    var errorCB = function(response) {
        this.setErrorMessage("wfError","Failed to create workflow: " + response);
    };
    this.http("POST", this.deployerURL, body, callback.bind(this), errorCB.bind(this));
};

// Executes an event and reloads the workflow
App.prototype.executeEvent = function(eventURL) {
    var body = JSON.stringify({username: this.username, value: true, token: this.token});
    var callback = function(response) {
        this.loadWorkflow(this.currentWorkflow[0], this.currentWorkflow[1], true);
        this.setErrorMessage("executeError", "");
    };
    var errorCB = function(response) {
        this.setErrorMessage("executeError", "Failed to execute event: " + response);
    };
    this.http("PUT", eventURL + "/executed", body, callback.bind(this), errorCB.bind(this));
};

// Get the executable events
App.prototype.getExecutables = function(events, callback) {
    var executables = [];
    for (var i = 0; i < events.length; i++) {
        if(events[i].executable == true){
            executables.push(events[i])
        }
    }
    callback(executables);
};

// Render list of workflows
App.prototype.renderWorkflows = function(wfIDs) {
    var workflows = document.getElementById("workflows");
    workflows.innerHTML = "";
    // workflowIDs.sort();
    var ul = workflows.appendChild(this.el("ul"));
    var helper = function(key) {
        return function(e) {
            e.preventDefault(); // Prevents changing url to #
            this.loadWorkflow(wfIDs[key].Title, wfIDs[key].ID);
        };
    }
    for (var key in wfIDs) {
        var li = workflows.appendChild(this.el("li"));
        var text = wfIDs[key].Title + " (ID: " + wfIDs[key].ID + ")"
        var a = li.appendChild(this.el("a", text));
        a.href = "#";
        a.addEventListener("click", helper(key).bind(this));
    }
};

// Load workflows and render them
App.prototype.loadWorkflows = function() {
    this.http("GET", this.storageURL + "/workflows/", null, this.renderWorkflows.bind(this));
};

// Render an array of events to the DOM
App.prototype.renderWorkflow = function(name, ID, events) {
    this.getExecutables(events, function(executables) {
        var workflow = document.getElementById("workflow");
        workflow.innerHTML = "";
        workflow.appendChild(this.el("p", "Workflow: " + name + " (ID: " + ID + ")"));
        var table = workflow.appendChild(this.el("table", null, "events"));
        var tr = table.appendChild(this.el("tr"));
        tr.appendChild(this.el("th", "Label"));
        tr.appendChild(this.el("th", "Included"));
        tr.appendChild(this.el("th", "Pending"));
        tr.appendChild(this.el("th", "Executed"));
        var helper = function(i) {
            return function(e) {
                e.preventDefault(); // Prevents changing url to #
                this.executeEvent(executables[i].id);
            };
        }
        for (var i = 0; i < executables.length; i++) {
            if(executables[i].roles == this.currentRole){
                tr = table.appendChild(this.el("tr"));
                tr.appendChild(this.el("td", executables[i].label, "event"));
                tr.appendChild(this.el("td", executables[i].included ? "True" : "False"));
                tr.appendChild(this.el("td", executables[i].pending ? "True" : "False"));
                tr.appendChild(this.el("td", executables[i].executed ? "True" : "False"));
                tr.addEventListener("click", helper(i).bind(this));
            }
        }
    }.bind(this));
    document.getElementById("main").classList.remove("hidden");
};

// Loads a workflow and renders it
App.prototype.loadWorkflow = function(name, ID, old) {
    this.currentWorkflow[0] = name;
    this.currentWorkflow[1] = ID;
    if(!old){
        url = this.authURL + "/users/" + this.username + "/roles";
        header = ["Token", this.token];
        callback = function(response) {
            this.roles = response;
            this.addSelect("select", response);
            this.currentRole = response[0];
            this.batchGet(ID, function(events) {
                this.currentEvents = events
                this.renderWorkflow(name, ID, events);
                this.loadWorkflows();
            }.bind(this));
        };
        this.http("GET", url, null, callback.bind(this), null, header);
    } else {
        this.batchGet(ID, function(events) {
            this.currentEvents = events
            this.renderWorkflow(name, ID, events);
            this.loadWorkflows();
        }.bind(this));
    }
};

// Runs the Application
App.prototype.run = function() {

    var body = document.body;

    document.getElementById("main").classList.add("hidden");
    document.getElementById("logout").classList.add("hidden");
    document.getElementById("sidebar").classList.add("hidden");

    this.loadWorkflows();

    // All drop events must be stopped (prevent default and stop propagation)
    // If drop events are not stopped, the dragged file will be loaded in the browser
    // Furthermore, to display the right cursors drop effect must be set
    var drop = document.getElementById("drop");
    var stop = function(e) { e.preventDefault(); e.stopPropagation(); };
    var toggle = function(e) { stop(e); drop.classList.toggle("hover"); };
    body.addEventListener("dragover", function(e) { stop(e);  e.dataTransfer.dropEffect = "none"});
    drop.addEventListener("dragover", function(e) { stop(e); e.dataTransfer.dropEffect = "copy"; });
    drop.addEventListener("dragenter", toggle, false);
    drop.addEventListener("dragleave", toggle, false);

    // Read the dropped file and commence deployment of workflow
    drop.addEventListener("drop", function(de) {
        toggle(de);
        var file = de.dataTransfer.files[0];
        var reader = new FileReader();
        reader.onload = function(le) {
            var title = document.getElementById("dcrName").value;
            if(title === "") {
                this.setErrorMessage("dcrError", "Failed to add DCR graph: Please write a name first!")
                return;
            }
            this.setErrorMessage("dcrError", "");
            var body = JSON.stringify({Title: title, XML: le.target.result});
            var url = this.storageURL + "/dcrs/";
            var errorCB = function(response) {
                this.setErrorMessage("dcrError", "Failed to add DCR graph: " + response);
            };
            this.http("POST", url, body, this.loadDcrs.bind(this), errorCB.bind(this));
        }.bind(this);
        reader.readAsText(file);
    }.bind(this), false);

    // Listen on selector
    var select = document.getElementById("select");
    select.addEventListener("change", function() {
        this.currentRole = select.value;
        this.renderWorkflow(this.currentWorkflow[0], this.currentWorkflow[1], this.currentEvents)
    }.bind(this), false);

    // Listen on login button
    var login = document.getElementById("loginButton");
    login.addEventListener("click", function() {
        var username = document.getElementById("username").value;
        var password = document.getElementById("password").value;
        this.login(username, password);
    }.bind(this), false);

    // Listen on logout button
    var logout = document.getElementById("logoutButton");
    logout.addEventListener("click", function() {
        this.logout();
    }.bind(this), false);

    // Listen on Create workflow button
    var createWF = document.getElementById("createWorkflowButton");
    createWF.addEventListener("click", function() {
        var name = document.getElementById("workflowName").value;
        if(name === "") {
                this.setErrorMessage("wfError", 
                    "Failed to create workflow: Please write a name first!");
                return;
        }
        this.setErrorMessage("wfError", "");
        var id = document.getElementById("dcrGraphs").options[dcrGraphs.selectedIndex].value;
        this.createWorkflow(name, id);
    }.bind(this), false);
};