#!/bin/bash
IMAGE=workflow-host
PORT=$1
BASE="http://ec2-52-28-69-195.eu-central-1.compute.amazonaws.com"
URL=$BASE:$PORT
NAME=$IMAGE$PORT
docker stop $NAME
docker rm $NAME
docker run --name $NAME -p $PORT:$PORT -e "PORT=$PORT" -e "URL=$URL" -i -d $IMAGE