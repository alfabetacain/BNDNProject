#!/bin/bash
NAME=workflow-auth
PORT=8001
docker stop $NAME
docker rm $NAME
docker run --name $NAME -p $PORT:$PORT -e "PORT=$PORT" -i -d $NAME