#!/bin/bash
NAME=workflow-deployer
PORT=8002
docker stop $NAME
docker rm $NAME
docker run --name $NAME -p $PORT:$PORT -e "PORT=$PORT" -i -d $NAME