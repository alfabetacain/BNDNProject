#!/bin/bash

# Postgres
pushd .
cd postgres
sh run.sh
popd

# Storage
pushd .
cd storage
sh build.sh
sh run.sh
popd

# Base
pushd .
cd base
sh build.sh
popd

# Auth
pushd .
cd auth
sh build.sh
sh run.sh
popd

# Deployer
pushd .
cd deployer
sh build.sh
sh run.sh
popd

# Hosts
pushd .
cd host
sh build.sh
for port in {8010..8012}; do sh run.sh $port; done
popd