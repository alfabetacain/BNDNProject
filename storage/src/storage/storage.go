package main

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

type Dcr struct {
	ID        int
	Title     string
	XML       string `sql:"type:text"`
	Sum       string `sql:"unique_index"`
	CreatedAt time.Time
}

type Workflow struct {
	ID        int
	DcrID     int
	Title     string
	CreatedAt time.Time
}

type Event struct {
	ID         int
	WorkflowID int
	Label      string
	CreatedAt  time.Time
}

type Log struct {
	ID        int
	EventID   int
	Action    string
	CreatedAt time.Time
}

type User struct {
	ID        int
	Username  string `sql:"unique_index"`
	Password  string
	Roles     []Role
	CreatedAt time.Time
}

type Role struct {
	ID     int
	UserID int
	Title  string
}

var (
	actions = map[string]bool{
		"executed": true,
	}
	db *gorm.DB
)

func handleDcrs(w http.ResponseWriter, r *http.Request) {

	split := strings.Split(strings.Trim(r.URL.Path, "/"), "/")

	switch r.Method {

	case "POST":
		decoder := json.NewDecoder(r.Body)
		d := Dcr{}
		err := decoder.Decode(&d)
		if err != nil {
			respond(w, "Failed to decode request", 400)
			return
		}
		d.Sum = fmt.Sprintf("%x", md5.Sum([]byte(d.XML)))
		if db.Where(&Dcr{Sum: d.Sum}).First(&d).RecordNotFound() {
			if err := db.Create(&d).Error; err != nil {
				respond(w, fmt.Sprintf("Failed to create entry in database: %s", err), 400)
				return
			}
		}
		respond(w, &d.ID, 200)
		return

	case "GET":
		switch len(split) {

		case 1:
			var ds []Dcr
			db.Find(&ds)
			respond(w, &ds, 200)
			return

		case 2:
			id, err := strconv.Atoi(split[1])
			if err != nil {
				respond(w, "Unable to parse id", 400)
				return
			}
			d := Dcr{ID: id}
			if db.Where(&d).First(&d).RecordNotFound() {
				respond(w, "DCR not found", 404)
				return
			}
			respond(w, &d, 200)
			return
		}

	default:
		respond(w, "Method not allowed", 405)
		return
	}

	respond(w, "Bad request", 400)
}

func handleWorkflows(w http.ResponseWriter, r *http.Request) {

	switch r.Method {

	case "POST":
		decoder := json.NewDecoder(r.Body)
		wf := Workflow{}
		err := decoder.Decode(&wf)
		if err != nil {
			respond(w, fmt.Sprintf("Failed to decode request: %s", err), 400)
			return
		}
		if err := db.Create(&wf).Error; err != nil {
			respond(w, fmt.Sprintf("Failed to create entry in database: %s", err), 400)
			return
		}
		respond(w, &wf.ID, 200)
		return

	case "GET":
		var wfs []Workflow
		db.Find(&wfs)
		respond(w, &wfs, 200)
		return

	default:
		respond(w, "Method not allowed", 405)
		return
	}

	respond(w, "Bad request", 400)
}

func handleEvents(w http.ResponseWriter, r *http.Request) {

	split := strings.Split(strings.Trim(r.URL.Path, "/"), "/")

	switch r.Method {

	case "POST":
		switch len(split) {

		case 1:
			decoder := json.NewDecoder(r.Body)
			e := Event{}
			err := decoder.Decode(&e)
			if err != nil {
				respond(w, fmt.Sprintf("Failed to decode request: %s", err), 400)
				return
			}
			if err := db.Create(&e).Error; err != nil {
				respond(w, fmt.Sprintf("Failed to create entry in database: %s", err), 400)
				return
			}
			respond(w, &e.ID, 200)
			return

		case 3:
			id, err := strconv.Atoi(split[1])
			if err != nil {
				respond(w, fmt.Sprintf("Unable to parse id: %s", err), 400)
				return
			}
			action := split[2]
			if _, valid := actions[action]; !valid {
				respond(w, fmt.Sprintf("Invalid action: %s", action), 403)
				return
			}
			l := Log{EventID: id, Action: action}
			if err := db.Create(&l).Error; err != nil {
				respond(w, fmt.Sprintf("Failed to create entry in database: %s", err), 400)
				return
			}
			fmt.Fprintf(w, "%d", l.ID)
			return
		}

	default:
		respond(w, "Method not allowed", 405)
		return
	}

	respond(w, "Bad request", 400)
}

func handleUsers(w http.ResponseWriter, r *http.Request) {

	split := strings.Split(strings.Trim(r.URL.Path, "/"), "/")

	switch r.Method {

	case "POST":
		decoder := json.NewDecoder(r.Body)
		u := User{}
		err := decoder.Decode(&u)
		if err != nil {
			respond(w, fmt.Sprintf("Failed to decode user: %s", err), 400)
			return
		}
		if err := db.Create(&u).Error; err != nil {
			respond(w, fmt.Sprintf("Failed to create user: %s", err), 400)
			return
		}
		respond(w, &u.Username, 200)
		return

	case "GET":
		var us []User
		db.Find(&us)
		for key, u := range us {
			var roles []Role
			db.Model(&u).Related(&roles)
			us[key].Roles = roles
		}
		respond(w, &us, 200)
		return

	case "PUT":
		if len(split) != 3 {
			break
		}
		username := split[1]
		property := split[2]
		u := User{Username: username}
		if db.Where(&u).First(&u).RecordNotFound() {
			respond(w, "User not found", 404)
			return
		}
		switch property {

		case "password":
			body, _ := ioutil.ReadAll(r.Body)
			u.Password = string(body)

		case "roles":
			decoder := json.NewDecoder(r.Body)
			err := decoder.Decode(&u.Roles)
			if err != nil {
				respond(w, fmt.Sprintf("Failed to decode roles: %s", err), 400)
				return
			}
			db.Where(&Role{UserID: u.ID}).Delete(Role{})

		default:
			respond(w, fmt.Sprintf("Can't put user property %s", property), 403)
			return
		}

		if err := db.Save(&u).Error; err != nil {
			respond(w, fmt.Sprintf("Failed to update user %s: %s", property, err), 400)
			return
		}
		respond(w, &u, 200)
		return

	default:
		respond(w, "Method not allowed", 405)
		return
	}

	respond(w, "Bad request", 400)
}

func respond(w http.ResponseWriter, i interface{}, status int) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
	j, err := json.Marshal(&i)
	if err != nil {
		http.Error(w, fmt.Sprintf("Failed to encode response: %s", err), 500)
		return
	}
	body := string(j)
	if status != 200 {
		http.Error(w, body, status)
	}
	fmt.Fprint(w, body)
}

func getDB(reset bool) *gorm.DB {

	args := fmt.Sprintf("sslmode=disable host=%s port=%s dbname=%s user=%s password=%s", os.Getenv("PGHOST"), os.Getenv("PGPORT"), os.Getenv("PGDATABASE"), os.Getenv("PGUSER"), os.Getenv("PGPASSWORD"))
	log.Printf("Connecting to postgres: %s\n", args)
	db, err := gorm.Open("postgres", args)
	if err != nil {
		log.Fatalf("%s\n", err)
	}

	db.DB()
	db.DB().Ping()
	db.DB().SetMaxOpenConns(100)
	db.DB().SetMaxIdleConns(50)

	if reset {
		log.Printf("Resetting the database...")
		db.Exec("DROP SCHEMA public CASCADE;")
		db.Exec("CREATE SCHEMA public;")
		db.AutoMigrate(&Dcr{}, &Workflow{}, &Event{}, &Log{}, &User{}, &Role{})
		db.Model(&Workflow{}).AddForeignKey("dcr_id", "dcrs", "RESTRICT", "RESTRICT")
		db.Model(&Event{}).AddForeignKey("workflow_id", "workflows", "RESTRICT", "RESTRICT")
		db.Model(&Log{}).AddForeignKey("event_id", "events", "RESTRICT", "RESTRICT")
		db.Model(&Role{}).AddForeignKey("user_id", "users", "RESTRICT", "RESTRICT")
	}

	return &db
}

func Run(reset bool) {

	db = getDB(reset)

	http.HandleFunc("/dcrs/", handleDcrs)
	http.HandleFunc("/workflows/", handleWorkflows)
	http.HandleFunc("/events/", handleEvents)
	http.HandleFunc("/users/", handleUsers)

	port := os.Getenv("PORT")
	log.Printf("Listening on port %s\n", port)
	http.ListenAndServe(":"+port, nil)
}
