package main

import (
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"testing"
	"time"
)

const (
	createError = `"Failed to create`
)

func TestMain(m *testing.M) {
	go Run(true)
	time.Sleep(1 * time.Second) // Wait for storage to get ready...
	os.Exit(m.Run())
}

func TestPostDcrs(t *testing.T) {
	post("/dcrs/", `{"Title": "TestPostDcrs1", "XML": "<test>1</test>"}`, 200, "1", t)
	post("/dcrs/", `{"Title": "TestPostDcrs2", "XML": "<test>2</test>"}`, 200, "2", t)
	post("/dcrs/", `{"Title": "TestPostDcrs3", "XML": "<test>1</test>"}`, 200, "1", t)
}

func TestGetDcrs(t *testing.T) {
	get("/dcrs/", 200, t)
}

func TestGetDcrsId(t *testing.T) {
	get("/dcrs/1", 200, t)
	get("/dcrs/3", 404, t)
}

func TestPostWorkflows(t *testing.T) {
	post("/workflows/", `{"Title": "TestPostWorkflows1", "DCRID": 1}`, 200, "1", t)
	post("/workflows/", `{"Title": "TestPostWorkflows2", "DCRID": 1}`, 200, "2", t)
	post("/workflows/", `{"Title": "TestPostWorkflows3", "DCRID": 3}`, 400, createError, t)
}

func TestGetWorkflows(t *testing.T) {
	get("/workflows/", 200, t)
}

func TestPostEvents(t *testing.T) {
	post("/events/", `{"Label": "TestPostEvents1", "WorkflowID": 1}`, 200, "1", t)
	post("/events/", `{"Label": "TestPostEvents2", "WorkflowID": 1}`, 200, "2", t)
	post("/events/", `{"Label": "TestPostEvents3", "WorkflowID": 3}`, 400, createError, t)
}

func TestPostEventsIdAction(t *testing.T) {
	post("/events/1/executed", "", 200, "1", t)
	post("/events/1/executed", "", 200, "2", t)
	post("/events/1/invalid", "", 403, `"Invalid action`, t)
	post("/events/3/executed", "", 400, createError, t)
}

func TestPostUsers(t *testing.T) {
	post("/users/", `{"Username": "TestPostUsers1", "Password": "TestPostUsers1", "Roles": [{"Title": "TestPostUsers1"}]}`, 200, `"TestPostUsers1"`, t)
	post("/users/", `{"Username": "TestPostUsers1", "Password": "TestPostUsers1", "Roles": [{"Title": "TestPostUsers1"}]}`, 400, createError, t)
}

func TestGetUsers(t *testing.T) {
	get("/users/", 200, t)
}

func TestPutUsersUsernamePassword(t *testing.T) {
	put("/users/TestPostUsers1/password", "TestPutUsersUsernamePassword1", 200, t)
}

func TestPutUsersUsernameRoles(t *testing.T) {
	put("/users/TestPostUsers1/roles", `[{"Title": "TestPutUsersUsernameRoles1"}]`, 200, t)
}

func post(path string, body string, code int, expected string, t *testing.T) {
	r, err := http.Post("http://0.0.0.0:8000"+path, "application/json", strings.NewReader(body))
	if err != nil {
		t.Errorf("Failed to post: %s", err)
		return
	}
	response, _ := ioutil.ReadAll(r.Body)
	if r.StatusCode != code {
		t.Errorf("Post returned unexpected status: %d, %s", r.StatusCode, response)
		return
	}
	if !strings.HasPrefix(string(response), expected) {
		t.Errorf("Expected %s to start with prefix %s", response, expected)
	}
}

func get(path string, code int, t *testing.T) {
	r, err := http.Get("http://0.0.0.0:8000" + path)
	if err != nil {
		t.Errorf("Failed to get: %s", err)
		return
	}
	if r.StatusCode != code {
		t.Errorf("Get returned unexpected status: %s", r.Status)
		return
	}
}

func put(path string, body string, code int, t *testing.T) {
	request, err := http.NewRequest("PUT", "http://0.0.0.0:8000"+path, strings.NewReader(body))
	r, err := http.DefaultClient.Do(request)
	if err != nil {
		t.Errorf("Failed to put: %s", err)
		return
	}
	response, _ := ioutil.ReadAll(r.Body)
	if r.StatusCode != code {
		t.Errorf("Put returned unexpected status: %d, %s", r.StatusCode, response)
	}
}
