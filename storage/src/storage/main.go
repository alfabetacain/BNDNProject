package main

import (
	"flag"
)

func main() {
	var reset = flag.Bool("reset", false, "resets the database when set to true")
	flag.Parse()
	Run(*reset)
}
