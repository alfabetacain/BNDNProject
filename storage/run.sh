#!/bin/bash
NAME=workflow-storage
PORT=8000
PG=workflow-postgres
docker stop $NAME
docker rm $NAME
docker run --name $NAME -p $PORT:$PORT -e "PORT=$PORT" --link $PG:pg -d $NAME