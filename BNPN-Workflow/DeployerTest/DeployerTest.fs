module DeployerTest

open System.IO
open NUnit.Framework
open FsUnit

open System.Xml
open System.Text.RegularExpressions

open Items
open XMLParser
open Deployer

let events =
    Map.empty.
        Add("Activity 0", {
            id = "Activity 0"
            label = "Zero"
            executed = false
            included = false
            pending = false
            preconditions = ["Activity 1"]
            conditions = ["Activity 1"]
            responses = ["Activity 1"]
            exclusions = ["Activity 1"]
            inclusions = ["Activity 1"]
            resources = []
            created = -1
            modified = -1
            roles = ["Activity 0"]
            executable = false
            lock = None
        }).
        Add("Activity 1", {
            id = "Activity 1"
            label = "One"
            executed = false
            included = false
            pending = false
            preconditions = ["Activity 0"]
            conditions = ["Activity 0"]
            responses = ["Activity 0"]
            exclusions = ["Activity 0"]
            inclusions = ["Activity 0"]
            resources = []
            created = -1
            modified = -1
            roles = ["Activity 0"]
            executable = false
            lock = None
        })

let workflow:Workflow =
    {
        id = ""
        title = "Test"
        roles = []
        events = events
    }

[<Test>]
let urlifyWorkflowTest () =
    let urls = [
        "http://eventhost.io/events/0"
        "http://eventhost.io/events/1"
    ]
    let urlified = Deployer.urlifyWorkflow urls workflow
    Assert.AreEqual(
        urlified.events.[urls.[0]].id,
        urls.[0]
    )
    Assert.AreEqual(
        urlified.events.[urls.[0]].conditions,
        [urls.[1]]
    )
    Assert.AreEqual(
        urlified.events.[urls.[1]].id,
        urls.[1]
    )
    Assert.AreEqual(
        urlified.events.[urls.[1]].conditions,
        [urls.[0]]
    )
    // Maybe add more tests, possibly redundant...
