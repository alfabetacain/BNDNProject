module Server

open System.Net
open System.Text

[<AbstractClass>]
type Server (port0:int) =
    do printfn "Connection limit: %d" ServicePointManager.DefaultConnectionLimit
    let hl = new HttpListener ()
    member this.port = port0
    member this.GetBody (req:HttpListenerRequest) =
        let is = req.InputStream
        let ce = req.ContentEncoding
        let sr = new System.IO.StreamReader(is, ce)
        let data = sr.ReadToEnd()
        is.Close ()
        data
    member this.Respond status (body:string) (res:HttpListenerResponse) =
        let txt = Encoding.ASCII.GetBytes(body)
        res.AddHeader("Access-Control-Allow-Origin", "*")
        res.AddHeader("Access-Control-Allow-Headers", "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With, Token, Password")
        res.AddHeader("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE, OPTIONS")
        res.ContentType <- "text/html"
        res.StatusCode <- status
        res.OutputStream.Write(txt, 0, txt.Length)
        res.OutputStream.Close()
        res.Close ()
    member this.RespondWithTuple (status, (body:string)) (res:HttpListenerResponse) =
        this.Respond status body res
    member this.RespondWithReason status reason (body: string) (res: HttpListenerResponse) =
        let txt = Encoding.ASCII.GetBytes(body)
        res.AddHeader("Access-Control-Allow-Origin", "*")
        res.AddHeader("Access-Control-Allow-Headers", "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With")
        res.AddHeader("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE, OPTIONS")
        res.ContentType <- "text/html"
        res.StatusCode <- status
        res.StatusDescription <- reason
        res.OutputStream.Write(txt, 0, txt.Length)
        res.OutputStream.Close()
        res.Close ()
    abstract member Handle : HttpListenerRequest -> HttpListenerResponse -> Async<unit>
    member this.Listen =
        hl.Prefixes.Add (sprintf "http://+:%d/" this.port)
        hl.Start()
        let task = Async.FromBeginEnd(hl.BeginGetContext, hl.EndGetContext)
        async {
            while true do
                let! ctx = task
                Async.Start(this.Handle ctx.Request ctx.Response)
        } |> Async.Start
    member this.Stop () =
        hl.Stop ()