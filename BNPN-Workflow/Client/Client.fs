﻿module Client

open System.Net
open Items
open Newtonsoft.Json
open System.Text.RegularExpressions

//method for splitting and preserving spaces when encased with "
let split (input: string) = 
    let inputChars = List.ofArray <| input.ToCharArray ()
    let rec helper awaitingQuote (chunk: string) = function
    | [] -> 
        if chunk.Length = 0 then []
        else chunk :: []
    | head::tail -> //some of then can probably be squashed together
        match (head, awaitingQuote) with
        | ('"', true) -> chunk :: helper false "" tail
        | ('"', false) -> helper true "" tail
        | (' ', true) -> helper true (chunk + " ") tail
        | (' ', _) -> 
            if chunk.Length = 0 then helper false "" tail
            else chunk :: helper false "" tail
        | (otherChar, _) -> helper awaitingQuote (chunk + (string otherChar)) tail
    helper false "" inputChars

let getAction (cmdList: string list) = 
    use client = new System.Net.WebClient ()
    client.DownloadString (List.head cmdList)

let upload (address: string) protocol data = 
    use client = new WebClient ()
    try
        client.UploadString (address,protocol,data)
    with
    | :? WebException as ex when (ex.Response :? HttpWebResponse) ->
        use response = ex.Response :?> HttpWebResponse
        sprintf "ERROR -> %d: %s" (int response.StatusCode) response.StatusDescription
    | excep -> sprintf "%s: %s" "ERROR" "Did you spell everything correctly?"

let uploadCommand protocol (cmdList: string list) = 
    match cmdList with
    | url::[] -> upload url protocol ""
    | url::almostEvent::[] -> 
        if System.IO.File.Exists (sprintf "%s" almostEvent) then
            let fileData = System.IO.File.ReadAllText (sprintf "%s" almostEvent)
            upload url protocol fileData
        else
            upload url protocol almostEvent
    | _ -> "Wrong parameters"

let readFile name = 
    if System.IO.File.Exists name then
        Some (System.IO.File.ReadAllText name)
    else 
        None

let parseUrls (text: string) = 
    JsonConvert.DeserializeObject<Map<string,string>> text

let urlToMaps (urls: string list) =  
    let helper acc (url: string) = 
        use client = new System.Net.WebClient ()
        let json = client.DownloadString url
        let event = JsonConvert.DeserializeObject<Event> json
        (event.label.ToUpper (), url) :: acc
    Map.ofList <| List.fold helper [] urls

let readUrls fileName = 
    match readFile fileName with
    | None -> Map.empty
    | Some text ->
        parseUrls text

let putify stateFile appendValue (id: string) = 
    let map = readUrls stateFile
    match Map.tryFind (id.ToUpper ()) map with
    | None -> upload (sprintf "%s/%s" id appendValue) "PUT" "true"
    | Some url -> upload (sprintf "%s/%s" url appendValue) "PUT" "true"

let putifyListParse stateFile appendValue (cmdList: string list) = 
    match cmdList with
    | head::_ -> putify stateFile appendValue head
    | _ -> "Need id or url"

let getEvent (url: string) =
    use client = new System.Net.WebClient ()
    try 
        let json = client.DownloadString url
        Some (JsonConvert.DeserializeObject<Event> json)
    with 
    | _ -> None

let getStringStateLine (event: Event) = 
    sprintf "\n%s: %s = %b | %s = %b | %s = %b | %s = %A\n" event.label "executed" event.executed "included" event.included "pending" event.pending "roles" event.roles

let showState stateFile (cmdList: string list) = 
    let map = readUrls stateFile
    if map.IsEmpty then "No state present"
    else 
        let stringify (url: string) = 
            match getEvent url with
            | None -> sprintf "%s: Not found!\n" url
            | Some event -> getStringStateLine event
        Map.fold (fun acc _ entry -> acc + (stringify entry)) "" <| map

let showEventState stateFile (id: string) = 
    let map = readUrls stateFile
    match Map.tryFind (id.ToUpper ()) map with
    | None -> "No such event"
    | Some url -> 
        match getEvent url with
        | None -> "No such event"
        | Some event -> getStringStateLine event

let showExecutable stateFile = 
    let map = readUrls stateFile
    if map.IsEmpty then "No state present"
    else 
        let helper acc label url =
            let request = sprintf "%s/executable" url
            let executable = getAction [request]
            if executable.Contains "False" then acc
            else acc + (sprintf "%s: %A\n" label executable)
        let exeList = Map.fold helper "" map
        if System.String.IsNullOrWhiteSpace exeList then "No executable events. Workflow finished"
        else exeList

let show stateFile = function
    | [] -> "Needs parameters"
    | (head: string)::tail -> 
        match head.ToUpper () with
        | "STATE" -> showState stateFile tail
        | "EXECUTABLE" -> showExecutable stateFile
        | id -> showEventState stateFile id

let execute stateFile (label: string list) =
    if label.Length <> 1 then "Usage: execute <event label>"
    else 
        let response = putify stateFile "executed" <| List.head label
        if response.Contains "ERROR" then response
        else sprintf "%s\n\nExecutables:\n%s" response <| showExecutable stateFile


let writeState stateFile (map: Map<string,string>) = 
    let json = JsonConvert.SerializeObject map
    System.IO.File.WriteAllText (stateFile, json)

let cleanState stateFile (cmdList: string list) = 
    if System.IO.File.Exists stateFile then 
        System.IO.File.Delete stateFile
        sprintf "Sucessfully deleted %s" stateFile
    else sprintf "No %s found" stateFile

let deploy stateFileName (cmdList: string list) = 
    if cmdList.Length <> 2 then "Usage: deploy <deployer address> <workflow xml file name>"
    else 
        let response = uploadCommand "POST" cmdList
        // printfn "%s" response
        if response.StartsWith "ERROR" then response
        else 
            let splitted = JsonConvert.DeserializeObject<string[]> response
            if splitted = null then "Failed to deserialize response"
            else
                let map = urlToMaps (List.ofArray splitted)
                writeState stateFileName map
                //make the response nice to print
                Map.fold (fun acc key elem -> acc + key + " -> " + elem + "\n") "" map

let cmdNames = ["DEPLOY"; "SHOW"; "EXECUTE"; "CLEAN"]

let descrips = ["Usage: deploy <deployer address> <workflow xml file name>"; "usage: show <entity name | executable | state>"; "usage: execute <entity name>";"usage: clean"]

let cmdActions = [deploy "state.txt"; show "state.txt"; execute "state.txt"; cleanState "state.txt"]

let run = 
    let cmds = List.zip cmdNames <| List.zip descrips cmdActions
    let commands = Map.ofList <| cmds
    let rec reader (input: string) = 
        let tempList = split input
        let cmdList = List.filter (fun (entry: string) -> not (System.String.IsNullOrWhiteSpace entry)) tempList
        match cmdList with 
        | [] -> 
            Map.iter (fun key value -> printfn "%s: %s" key (fst value)) commands
        | head::tail -> 
            match Map.tryFind (head.ToUpper ()) commands with
            | None -> printfn "Could not understand command"
            | Some (descrip,cmd) -> 
                match tail with
                | [help] when help.ToUpper () = "HELP" -> printfn "%s" descrip
                | t -> printfn "%s" <| cmd t
        printf "\n>"
        reader <| System.Console.ReadLine ()
    reader ""
    0 // return an integer exit code