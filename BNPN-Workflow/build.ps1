#you must have ilmerge installed for this to work
#you must build the solution as release in visual studio first

if (test-path "final") {
    rmdir "final" -recurse -force
}

mkdir "final"

ilmerge /allowdup /out:final/deployer.exe .\Deployer\bin\Release\Deployer.exe .\Deployer\bin\Release\Host.exe .\Deployer\bin\Release\Newtonsoft.Json.dll .\Deployer\bin\Release\Server.dll .\Deployer\bin\Release\XMLParser.dll

ilmerge /out:final/client.exe .\Client\bin\Release\Client.exe .\Client\bin\Release\Host.exe .\Client\bin\Release\Newtonsoft.Json.dll .\Client\bin\Release\Server.dll

ilmerge /out:final/host.exe .\Host\bin\Release\Host.exe .\Host\bin\Release\Newtonsoft.Json.dll .\Host\bin\Release\Server.dll .\Host\bin\Release\nunit.framework.dll

copy "..\Brazil graph.xml" "final"

del "final\*.pdb"
