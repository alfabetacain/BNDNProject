#!/bin/bash
pushd .
cd DeployerTest
sh run.sh
popd
pushd .
cd HostTest
sh run.sh
popd
pushd .
cd IntegrationTest
sh run.sh
popd
pushd .
cd XMLParserTest
sh run.sh
popd