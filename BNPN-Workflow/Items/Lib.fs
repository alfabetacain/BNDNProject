﻿module Lib

open Newtonsoft.Json
open Items
open System.Net
open System.Text

let tryParseJson<'a> body =
    let rules = new JsonSerializerSettings ()
    rules.MissingMemberHandling <- MissingMemberHandling.Error
    try
        let value = JsonConvert.DeserializeObject<'a> (body,rules)
        if box value = null then Failure <| sprintf "Failed to deserialize %s" body
        else Success value
    with _ -> Failure <| sprintf "Failed to deserialize %s" body

let tryUpload (address: string) protocol data =
    use client = new WebClient ()
    try
        let res = client.UploadString (address,protocol,data)
        Success res
    with
    | :? WebException as ex when (ex.Response :? HttpWebResponse) ->
        let response = ex.Response :?> HttpWebResponse
        let (is: System.IO.Stream) = response.GetResponseStream ()
        let sr = new System.IO.StreamReader(is)
        printf "%s" <| sr.ReadToEnd()
        Failure <| (int response.StatusCode, response.StatusDescription)
    | excep -> Failure <| (500,string excep)