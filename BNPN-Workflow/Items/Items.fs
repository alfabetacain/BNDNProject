﻿module Items

type Event =
    {
        id: string
        label: string
        executed: bool
        included: bool
        pending: bool
        executable: bool
        conditions: string list
        responses: string list
        exclusions: string list
        inclusions: string list
        preconditions: string list
        resources: string list
        created: int
        modified: int
        roles: string list
        lock: string option
    }
    // This was the best solution for setters I could find, my sources were:
    // http://fsharpforfunandprofit.com/posts/records/
    // https://msdn.microsoft.com/en-us/library/dd233184.aspx
    member this.setExecuted bool = {this with executed = bool}
    member this.setCreated time = {this with created = time}
    member this.setIncluded bool = {this with included = bool}
    member this.setPending bool = {this with pending = bool}
    member this.setState exe inc pen = {this with executed = exe; included = inc; pending = pen}
    member this.setPreconditions preCond = {this with preconditions = preCond}
    member this.setExecutable value = {this with executable = value}
    member this.setLock value = {this with lock = value}
    override this.ToString () = sprintf "%A" this

type PUT_DTO = {
    username: string
    value: bool
    token: string
}

type LOCK_DTO = {
    id: string
    value: bool
    shouldPropagateToPrecond: bool
}

type PRECONDITION_DTO = { 
    role: string
    name: string
    shouldAdd: bool
}

type Role = {
    title: string
}
type User = {
    username: string
    password: string
    roles: Role list
}
type SimpleUser = {
    username: string
    password: string
    roles: string list
}

type Workflow =
    {
        id: string
        title: string
        roles: list<string>
        events: Map<string,Event>
    }

type WorkflowDTO =
    {
        id: string
        title: string
        roles: list<string>
        events: list<string>
    }
type DcrIdDTO =
    {
        DCRID: int
        Title: string
    }
type DcrDTO =
    {
        ID: string
        Title: string
        XML: string
        Sum: string
        CreatedAt: string
    }

type YetAnotherDto = {
    Label: string
    WorkflowID: int
}

type DcrToStorage = {
    Title: string
    XML: string
}
type Result<'TSuccess, 'TFailure> =
    | Success of 'TSuccess
    | Failure of 'TFailure
