open System

open Auth

[<EntryPoint>]
let main args =
    let defaults = [
        ("PORT", "8001");
        ("STORAGE", "http://ec2-52-28-69-195.eu-central-1.compute.amazonaws.com:8000");
    ]
    let getEnv key value =
        match Environment.GetEnvironmentVariable key with
        | null -> value
        | env -> env
    let input = defaults |> Map.ofList |> Map.map getEnv
    let auth = new Auth(Int32.Parse input.["PORT"], input.["STORAGE"])

    auth.Init

    let users = auth.GetUsers

    // Users for testing if not already present
    if not <| users.Contains "john" then auth.CreateUser "john" "password" ["First receptionist";"Second receptionist";"Specialist";"Auditor";"Caseworker";"it";"Mobile consultant";"Customer";"Intern"] |> ignore
    if not <| users.Contains "reception" then auth.CreateUser "reception" "password" ["First receptionist";"Second receptionist"] |> ignore
    if not <| users.Contains "reception1" then auth.CreateUser "reception1" "password" ["First receptionist"] |> ignore
    if not <| users.Contains "reception2" then auth.CreateUser "reception2" "password" ["Second receptionist"] |> ignore
    if not <| users.Contains "muller" then auth.CreateUser "muller" "password" ["Specialist"] |> ignore
    if not <| users.Contains "smith" then auth.CreateUser "smith" "password" ["Auditor"] |> ignore

    auth.Listen
    printfn "Press return to exit..."
    Console.ReadLine() |> ignore
    0