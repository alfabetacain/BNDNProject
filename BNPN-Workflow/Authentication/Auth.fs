module Auth

open System
open System.Net
open System.Text
open System.IO
open System.Text.RegularExpressions
open System.Collections.Concurrent
open Newtonsoft.Json
open Items
open Server

let seperator = "!!"

// For regexing in matches
let (|Regex|_|) pattern input =
    let m = Regex.Match(input, pattern)
    if m.Success then Some(List.tail [ for g in m.Groups -> g.Value ])
    else None

// For hashing tokens. Source http://tryfs.net/snippets/snippet-iv
let supersecretpasswordkey = [|178uy; 121uy; 203uy; 140uy; 158uy; 109uy; 106uy; 138uy; 78uy; 218uy|]
let xorer (key:byte []) = (fun i c -> c ^^^ key.[i%key.Length])

let encr (s:string) (key:byte []) = s |> Encoding.Default.GetBytes |> Array.mapi (xorer key) |> Convert.ToBase64String
let decr (s:string) (key:byte []) = s |> Convert.FromBase64String |> Array.mapi (xorer key) |> Encoding.Default.GetString

let generateKey (rnd:System.Random) count = List.toArray <| List.init count (fun _ -> byte (rnd.Next ()))


let rolesToStrings = List.map (fun (role:Role)->role.title)
let stringsToRoles = List.map (fun role->({title=role}:Role))

type Auth (port0:int, storage0:string) =
    inherit Server(port0)
    let storage = storage0
    let users = new ConcurrentDictionary<string,string>()
    let roles = new ConcurrentDictionary<string,string list>()
    let keys = new ConcurrentDictionary<string,byte array>()
    let tokens = new ConcurrentDictionary<string,string>()
    let tokenDuration = 20.0;
    let keysize = 2048
    let client = new System.Net.WebClient()

    member this.ValidatePassword username password = users.ContainsKey(username) && (users.Item username = password)
    member this.ValidateToken username token =
        if not <| tokens.ContainsKey(username) then false
        else
            if not <| (tokens.Item username = token) then false
            else
                if token = "system-token" then true // System token don't expire
                else
                    let now = System.DateTime.UtcNow;
                    let decrypted = decr <| tokens.Item username <| keys.Item username
                    let (success,time) = System.DateTime.TryParse((decrypted).Split([|seperator|],StringSplitOptions.RemoveEmptyEntries).[1]);
                    (now - time).TotalMinutes < tokenDuration // Expire tokens after x minutes

    member this.CreateUserInStorage username password rolies storage =
        if tokens.ContainsKey(username)
        then
            false
        else
            let mutable result = users.TryAdd(username,password) && roles.TryAdd(username,rolies) && tokens.TryAdd(username,"") && keys.TryAdd(username,generateKey (System.Random()) keysize)
            if not result then
                users.TryRemove(username) |> ignore
                roles.TryRemove(username) |> ignore
                tokens.TryRemove(username) |> ignore
                keys.TryRemove(username) |> ignore
            else if storage <> "" then
                let user:User = {
                    username = username
                    password = encr <| password <| supersecretpasswordkey
                    roles = stringsToRoles rolies
                }
                try
                    client.UploadString(Uri (storage+"/users/"),JsonConvert.SerializeObject user) |> ignore
                with
                | _ -> 
                    users.TryRemove(username) |> ignore
                    roles.TryRemove(username) |> ignore
                    tokens.TryRemove(username) |> ignore
                    keys.TryRemove(username) |> ignore
                    result <- false
            result
    member this.CreateUser username password rolies = this.CreateUserInStorage username password rolies storage
    member this.CreateUserFromObject (user:User) storage = this.CreateUserInStorage user.username user.password (rolesToStrings user.roles) storage

    member this.Init = 
        // Hard codes system user with no password
        tokens.TryAdd("system","system-token") |> ignore
        roles.TryAdd("system",["system-role"]) |> ignore

        let result = client.DownloadString(storage+"/users/")
        if result <> "null" then
            let storageusers = JsonConvert.DeserializeObject<User list> result
            let decryptedusers = List.map (fun (user:User) -> ({ username = user.username
                                                                 password = decr <| user.password <| supersecretpasswordkey
                                                                 roles = user.roles }:User)) storageusers
            List.forall (fun user -> this.CreateUserFromObject user "") (decryptedusers) |> ignore
        ()

    member this.GetUsers = users.Keys

    override this.Handle (req:HttpListenerRequest) (resp:HttpListenerResponse) = async {

        if req.HttpMethod = "OPTIONS"
        then this.Respond 200 "Success" resp
        else
            match req.RawUrl with
            | Regex @"/users/(.+)/token/?" [username] ->
                match req.HttpMethod with
                | "GET" ->
                    if this.ValidatePassword username <| req.Headers.Get "Password"
                    then
                        let newkey = generateKey (System.Random()) keysize
                        keys.TryUpdate(username, newkey, keys.Item username) |> ignore

                        let timestamp = System.DateTime.UtcNow.ToString();
                        let newtoken = encr <| username+seperator+timestamp <| keys.Item username
                        tokens.TryUpdate(username, newtoken, tokens.Item username) |> ignore

                        this.Respond 200 (JsonConvert.SerializeObject(newtoken)) resp
                    else this.Respond 401 "Unauthorized" resp
                | _ -> this.Respond 405 "Method Not Allowed" resp
            | Regex @"/users/(.+)/roles/?" [username] ->
                match req.HttpMethod with
                | "GET" ->
                    if this.ValidateToken username <| req.Headers.Get "Token"
                    then this.Respond 200 (JsonConvert.SerializeObject(roles.Item username)) resp
                    else this.Respond 401 "Unauthorized" resp
                | "PUT" ->
                    let newroles = JsonConvert.DeserializeObject<string list>(this.GetBody req)
                    if this.ValidateToken username <| req.Headers.Get "Token"
                    then
                        let oldroles = roles.Item username
                        if roles.TryUpdate(username, newroles, oldroles)
                        then
                            try 
                                let url = storage+"/users/"+username+"/roles/"
                                client.UploadString(Uri (url), "PUT", JsonConvert.SerializeObject (stringsToRoles newroles)) |> ignore 
                                this.Respond 200 "OK" resp
                            with
                                | :? System.Net.WebException as e -> 
                                    let res = e.Response:?>HttpWebResponse
                                    roles.TryUpdate(username, oldroles, newroles) |> ignore
                                    this.Respond (int res.StatusCode) res.StatusDescription resp
                        else this.Respond 400 "Bad Request" resp
                    else this.Respond 401 "Unauthorized" resp
                | _ -> this.Respond 405 "Method Not Allowed" resp
            | Regex @"/users/(.+)/password/?" [username] ->
                match req.HttpMethod with
                | "PUT" ->
                    let newpass = this.GetBody req
                    if this.ValidatePassword username <| req.Headers.Get "Password"
                    then
                        let oldpass = users.Item username
                        if users.TryUpdate(username, newpass, oldpass)
                        then 
                            try 
                                let encryptedPass = encr newpass supersecretpasswordkey
                                let url = storage+"/users/"+username+"/password/"
                                client.UploadString(Uri (url), "PUT", encryptedPass) |> ignore 
                                this.Respond 200 "OK" resp
                            with
                                | :? System.Net.WebException as e -> 
                                    let res = e.Response:?>HttpWebResponse
                                    users.TryUpdate(username, oldpass, newpass) |> ignore
                                    this.Respond (int res.StatusCode) res.StatusDescription resp
                        else this.Respond 400 "Bad Request" resp
                    else this.Respond 401 "Unauthorized" resp
                | _ -> this.Respond 405 "Method Not Allowed" resp
            | Regex @"/users/?" [] ->
                match req.HttpMethod with
                | "POST" ->
                    let user = JsonConvert.DeserializeObject<SimpleUser>(this.GetBody req)
                    let result = this.CreateUser user.username user.password user.roles

                    if result
                    then this.Respond 201 "Created" resp
                    else this.Respond 400 "Bad Request" resp
                | _ -> this.Respond 405 "Method Not Allowed" resp
            | _ -> this.Respond 404 "Not found" resp
    }