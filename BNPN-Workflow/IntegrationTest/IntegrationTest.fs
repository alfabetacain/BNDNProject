module IntegrationTest

open NUnit.Framework
open FsUnit
open System.Xml
open Host
open XMLParser
open Server
open Deployer
open Client
open Items
open System.Text.RegularExpressions
open Newtonsoft.Json
open System.Collections.Concurrent
open System.Net
open System.Web

let storageUrl = "http://ec2-52-28-69-195.eu-central-1.compute.amazonaws.com:8000"
let authUrl = "http://localhost:8001"
let deployerUrl = "http://localhost:8002"
let host0Url = "http://localhost:8010"
let host1Url = "http://localhost:8011"
let host2Url = "http://localhost:8012"
let standardSubscribeToDeploy = serverDeployer deployerUrl
let standardLogger = serverLog storageUrl
let brazilGraph = System.IO.File.ReadAllText "Brazil graph.xml"

let setupPairNoHippo () =
    let deployer = new Deployer(8002, false, storageUrl)
    deployer.Listen
    let host0 = new Host(8010, host0Url, standardLogger, serverAuth authUrl, standardSubscribeToDeploy)
    let stateFile = "state.txt"
    host0.Listen
    (deployer, host0)

let setupTwoHostsNoHippo () =
    let deployer = new Deployer(8002, false, storageUrl)
    deployer.Listen
    let host0 = new Host(8010, host0Url, standardLogger, serverAuth authUrl, standardSubscribeToDeploy)
    let host1 = new Host(8011, host1Url, standardLogger, serverAuth authUrl, standardSubscribeToDeploy)
    host0.Listen
    host1.Listen
    (deployer, host0, host1)

let setupThreeHostsNoHippo () =
    let deployer = new Deployer(8002, false, storageUrl)
    deployer.Listen
    let host0 = new Host(8010, host0Url, standardLogger, serverAuth authUrl, standardSubscribeToDeploy)
    let host1 = new Host(8011, host1Url, standardLogger, serverAuth authUrl, standardSubscribeToDeploy)
    let host2 = new Host(8012, host2Url, standardLogger, serverAuth authUrl, standardSubscribeToDeploy)
    host0.Listen
    host1.Listen
    host2.Listen
    (deployer, host0, host1, host2)

let setupNoHippo () =
    let deployer = new Deployer(8002, false, storageUrl)
    deployer.Listen
    let host0 = new Host(8010, host0Url, standardLogger, serverAuth authUrl, standardSubscribeToDeploy)
    let stateFile = "state.txt"
    host0.Listen

let stop (deployer: Server, host: Server) =
    deployer.Stop ()
    host.Stop ()

let stop2 (deployer: Server, host0: Server, host1: Server) =
    deployer.Stop ()
    host0.Stop ()
    host1.Stop()

let stop3 (deployer: Server, host0: Server, host1: Server, host2: Server) =
    deployer.Stop ()
    host0.Stop ()
    host1.Stop()
    host2.Stop()

let stateFile = "state.txt"

let deployTestState url flowFile =
    Client.deploy stateFile [url; flowFile]

let upload dcr =
    try 
        let client = new WebClient()
        let (dcrThingie: DcrToStorage) = { Title = "hello world"; XML = dcr }
        let storageUrl = "http://ec2-52-28-69-195.eu-central-1.compute.amazonaws.com:8000/dcrs/"
        int <| client.UploadString(storageUrl,"POST", JsonConvert.SerializeObject dcrThingie)  
    with
    | :? WebException as e ->
        let res = e.Response :?> HttpWebResponse
        failwith <| string e
    | e -> failwith <| string e
[<Test>]
let ``Deploy Two workflows with two hosts running No Hippo`` () =
    let servers = setupTwoHostsNoHippo ()
    let client = new System.Net.WebClient ()
    let wfId = upload brazilGraph
    let wfDto = { Title = "hello world"; DCRID = wfId }
    let a = client.UploadString (deployerUrl, "POST", JsonConvert.SerializeObject wfDto)
    let b = client.UploadString (deployerUrl, "POST", JsonConvert.SerializeObject wfDto)
    let response = client.DownloadString (host0Url + "/eventCount")
    let response2 = client.DownloadString (host1Url + "/eventCount")
    printfn "%s" response
    printfn "%s" response2
    stop2 servers
    Assert.AreEqual ("19", response)

[<Test>]
let ``Deploy Three workflows with two hosts running No Hippo`` () =
    let servers = setupTwoHostsNoHippo ()
    let client = new System.Net.WebClient ()
    let wfId = upload brazilGraph
    let wfDto = { Title = "hello world"; DCRID = wfId }
    let a = client.UploadString (deployerUrl, "POST", JsonConvert.SerializeObject wfDto)
    let b = client.UploadString (deployerUrl, "POST", JsonConvert.SerializeObject wfDto)
    let c = client.UploadString (deployerUrl, "POST", JsonConvert.SerializeObject wfDto)
    let response = client.DownloadString (host0Url + "/eventCount")
    let response2 = client.DownloadString (host1Url + "/eventCount")
    printfn "%s" response
    printfn "%s" response2
    stop2 servers
    Assert.AreEqual ("38", response2) //response |> should contain "Success"

[<Test>]
let ``Deploy three workflows with three hosts running No Hippo`` () =
    let servers = setupThreeHostsNoHippo ()
    let client = new System.Net.WebClient ()
    let wfId = upload brazilGraph
    let wfDto = { Title = "hello world"; DCRID = wfId }
    printf "%A" wfId
    let a = client.UploadString (deployerUrl, "POST", JsonConvert.SerializeObject wfDto)
    let b = client.UploadString (deployerUrl, "POST", JsonConvert.SerializeObject wfDto)
    let c = client.UploadString (deployerUrl, "POST", JsonConvert.SerializeObject wfDto)
    let response = client.DownloadString (host0Url + "/eventCount")
    let response2 = client.DownloadString (host1Url + "/eventCount")
    let response3 = client.DownloadString (host2Url + "/eventCount")
    printfn "%s" response
    printfn "%s" response2
    printfn "%s" response3
    stop3 servers
    Assert.AreEqual ("19", response) //response |> should contain "Success"

[<Test>]
let ``Multiple Ids are different No Hippo`` () =
    let servers = setupThreeHostsNoHippo ()
    let client = new System.Net.WebClient ()
    let wfId = upload brazilGraph
    let wfDto = { Title = "hello world"; DCRID = wfId }
    
    let a = client.UploadString (deployerUrl, "POST", JsonConvert.SerializeObject wfDto)
    let b = client.UploadString (deployerUrl, "POST", JsonConvert.SerializeObject wfDto)
    let c = client.UploadString (deployerUrl, "POST", JsonConvert.SerializeObject wfDto)

    Assert.AreNotEqual(a, b);
    Assert.AreNotEqual(b, c);
    Assert.AreNotEqual(c, b);

    stop3 servers

[<Test>]
let ``Workflow urls are valid No Hippo`` () =
    let servers = setupThreeHostsNoHippo ()
    let client = new System.Net.WebClient ()
    let fileData = System.IO.File.ReadAllText "Brazil graph.xml"
    let urls = [host0Url;host1Url;host2Url];
    let wfId = upload brazilGraph
    let wfDto = { Title = "hello world"; DCRID = wfId }

    let workflowJson = client.UploadString (deployerUrl, "POST", JsonConvert.SerializeObject wfDto)
    let workflow = JsonConvert.DeserializeObject<WorkflowDTO> workflowJson

    let any = List.fold (fun acc url -> acc || Regex.IsMatch(url,urls.[0]) || Regex.IsMatch(url,urls.[1])|| Regex.IsMatch(url,urls.[2])) false <| workflow.events
    Assert.AreEqual(true,any)

    stop3 servers

[<Test>]
let ``DownloadString contains all roles for Workflow No Hippo`` () =
    let servers = setupThreeHostsNoHippo ()
    let client = new System.Net.WebClient ()
    let wfId = upload brazilGraph
    let wfDto = { Title = "hello world"; DCRID = wfId }
    let urls = [host0Url;host1Url;host2Url];

    let workflowJson = client.UploadString (deployerUrl, "POST", JsonConvert.SerializeObject wfDto)
    let workflow = JsonConvert.DeserializeObject<WorkflowDTO> workflowJson
    let events = List.map (fun (elem:string) -> JsonConvert.DeserializeObject<Event> (client.DownloadString elem)) workflow.events

    let doc = new XmlDocument()
    doc.LoadXml <| System.IO.File.ReadAllText "Brazil graph.xml"
    let actualWorkflow = XMLParser.getWorkflow doc
    let actualWorkflowEvents = Map.fold (fun a _ elem -> Map.add elem.label elem a) Map.empty actualWorkflow.events

    let check (elem:Event) =
        match Map.tryFind elem.label actualWorkflowEvents with
        | None ->
            printf "%A" elem.label
            false
        | Some value ->
            value.pending = elem.pending &&
            value.executed = elem.executed &&
            value.included = elem.included

    Assert.True(List.forall check events)

    stop3 servers

[<Test>]
let ``cannot execute with wrong token`` () =
    //arrange
    let workflow = new ConcurrentDictionary<string,Event>()
    let event1 = {
        id = "http://localhost:8010/events/1"
        label = "main event"
        executed = false
        included = true
        pending = false
        conditions = []
        responses = []
        exclusions = []
        inclusions = []
        preconditions = []
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = false
        lock = None
    }
    let client = new WebClient ()
    let host = new Host (8010, host0Url, standardLogger, serverAuth authUrl, standardSubscribeToDeploy)
    let listen = async {
        try
            host.Listen
        with
            _ -> ()
         }
    Async.Start listen
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event1)) |> ignore
    let addDTO = {
        username = "system"
        value = true
        token = "psyched"
        } //,true)
    //act
    let json = JsonConvert.SerializeObject addDTO
    try
        client.UploadString ((sprintf "%s/%s" event1.id "executed"), "PUT", json) |> ignore
        host.Stop ()
        Assert.Fail ()
    with
        | :? System.Net.WebException as excep ->
            host.Stop ()
            let resp = excep.Response :?> HttpWebResponse
            Assert.AreEqual(401, int resp.StatusCode)
        | _ ->
            host.Stop ()
            Assert.Fail ()