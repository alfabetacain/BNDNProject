#!/bin/bash

fsharpc ../Server/Server.fs ../Items/Items.fs ../Host/Host.fs ../XMLParser/XMLParser.fs ../Deployer/Deployer.fs ../Client/Client.fs IntegrationTest.fs --lib:../Library/ -r:NUnit.2.6.3/lib/nunit.framework.dll -r:FsUnit.1.3.0.1/Lib/Net40/FsUnit.NUnit.dll -r:Newtonsoft.Json.6.0.8/lib/net45/Newtonsoft.Json.dll --nologo -a
export MONO_PATH="../Library/NUnit.2.6.3/lib/:../Library/FsUnit.1.3.0.1/Lib/Net40/:../Library/Newtonsoft.Json.6.0.8/lib/net45/"
mono ../Library/NUnit.Runners.2.6.3/tools/nunit-console.exe IntegrationTest.dll --nologo