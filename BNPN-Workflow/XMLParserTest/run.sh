#!/bin/bash

fsharpc ../Items/Items.fs ../XMLParser/XMLParser.fs XMLParserTest.fs --lib:../Library/ -r:NUnit.2.6.3/lib/nunit.framework.dll -r:FsUnit.1.3.0.1/Lib/Net40/FsUnit.NUnit.dll --nologo -a
export MONO_PATH="../Library/NUnit.2.6.3/lib/:../Library/FsUnit.1.3.0.1/Lib/Net40/"
mono ../Library/NUnit.Runners.2.6.3/tools/nunit-console.exe XMLParserTest.dll -nologo