﻿module XMLParserTest

open NUnit.Framework
open FsUnit

open System.Xml

open Items
open XMLParser

let doc = new XmlDocument()
doc.Load "test.xml"

[<Test>]
let getTitleTest () =
    Assert.AreEqual("Test workflow", XMLParser.getTitle doc)

[<Test>]
let getIdsTest () =
    let expected = Seq.toList (seq { for i in 0 .. 12 do yield sprintf "Activity %d" i })
    let actual = XMLParser.getIds doc
    Assert.True(List.forall2 (fun a b -> a = b) expected actual )

[<Test>]
let getNameTest () =
    Assert.AreEqual("Included", XMLParser.getName doc "Activity 0")

[<Test>]
let getExecutedTest () =
    Assert.AreEqual(false, XMLParser.getExecuted doc "Activity 11")
    Assert.AreEqual(true, XMLParser.getExecuted doc "Activity 2")

[<Test>]
let getIncludedTest () =
    Assert.AreEqual(false, XMLParser.getIncluded doc "Activity 11")
    Assert.AreEqual(true, XMLParser.getIncluded doc "Activity 0")

[<Test>]
let getPendingTest () =
    Assert.AreEqual(false, XMLParser.getPending doc "Activity 11")
    Assert.AreEqual(true, XMLParser.getPending doc "Activity 1")

[<Test>]
let getConditionsTest () =
    Assert.AreEqual([], XMLParser.getConditions doc "Activity 4")
    Assert.AreEqual(["Activity 4"; "Activity 3"], XMLParser.getConditions doc "Activity 12")

[<Test>]
let getPreconditionsTest () =
    Assert.AreEqual([], XMLParser.getPreconditions doc "Activity 11")
    Assert.AreEqual(["Activity 3"; "Activity 12"], XMLParser.getPreconditions doc "Activity 4")

[<Test>]
let getResponsesTest () =
    Assert.AreEqual([], XMLParser.getResponses doc "Activity 11")
    Assert.AreEqual(["Activity 6"; "Activity 5"], XMLParser.getResponses doc "Activity 12")

[<Test>]
let getExclusionsTest () =
    Assert.AreEqual([], XMLParser.getExclusions doc "Activity 11")
    Assert.AreEqual(["Activity 10"; "Activity 9"], XMLParser.getExclusions doc "Activity 12")

[<Test>]
let getInclusionsTest () =
    Assert.AreEqual([], XMLParser.getInclusions doc "Activity 11")
    Assert.AreEqual(["Activity 8"; "Activity 7"], XMLParser.getInclusions doc "Activity 12")

[<Test>] 
let getRolesTest () =
    Assert.AreEqual([], XMLParser.getRole doc "Activity 11")
    Assert.AreEqual(["Doctor"], XMLParser.getRole doc "Activity 12")

[<Test>]
let getAllRolesTest () =
    Assert.AreEqual(["Doctor";"Nurse";"Receptionist"], XMLParser.getAllRoles doc)

[<Test>]
let getEventTest () =
    let everything = {
        id = "Activity 4"
        label = "Conditioned"
        executed = false
        included = true
        pending = false
        preconditions = ["Activity 3"; "Activity 12"]
        conditions = []
        responses = []
        exclusions = []
        inclusions = []
        resources = []
        created = -1
        modified = -1
        roles = []
        executable = false
        lock = None
    }
    Assert.AreEqual(everything, XMLParser.getEvent doc "Activity 4")
    let empty = {
        id = "Activity 11"
        label = "Empty"
        executed = false
        included = false
        pending = false
        preconditions = []
        conditions = []
        responses = []
        exclusions = []
        inclusions = []
        resources = []
        created = -1
        modified = -1
        roles = []
        executable = false
        lock = None
    }
    Assert.AreEqual(empty, XMLParser.getEvent doc "Activity 11")

[<Test>]
let getWorkflowTest () =
    let workflow = XMLParser.getWorkflow doc
    // Check if the workflow has the correct title
    Assert.AreEqual("Test workflow", workflow.title)
    // Check if the workflow has the correct list of roles
    Assert.AreEqual(["Doctor";"Nurse";"Receptionist"], workflow.roles)
    // Check if the map of events contain the expected set of keys
    let expected = seq { for i in 0 .. 12 do yield sprintf "Activity %d" i }
    Assert.True(Seq.forall (fun key -> Map.containsKey key workflow.events) expected)
    // Check if the keys in the map of events point at the correct events
    Assert.True(Map.forall (fun key (value:Event) -> key = value.id) workflow.events)