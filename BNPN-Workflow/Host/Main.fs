open System

open Host

[<EntryPoint>]
let main args =
    let defaults = [
        ("PORT", "8010");
        ("URL","http://localhost:8010");
        ("STORAGE", "http://ec2-52-28-69-195.eu-central-1.compute.amazonaws.com:8000");
        ("AUTH", "http://localhost:8001");
        ("DEPLOYER", "http://localhost:8002");
    ]
    let getEnv key value =
        match Environment.GetEnvironmentVariable key with
        | null -> value
        | env -> env
    let input = defaults |> Map.ofList |> Map.map getEnv
    let authenticator = serverAuth input.["AUTH"]
    let subscribeToDeployer = serverDeployer input.["DEPLOYER"]
    let logger = serverLog input.["STORAGE"]
    let host = new Host(Int32.Parse input.["PORT"], input.["URL"], logger, authenticator, subscribeToDeployer)
    host.Listen
    printfn "Press return to exit..."
    Console.ReadLine() |> ignore
    0