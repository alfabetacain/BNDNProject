module Host

open System
open System.Net
open System.Text
open System.IO
open System.Text.RegularExpressions
open System.Collections.Concurrent
open Newtonsoft.Json
open Items
open Lib
open Server
//
//let tryParseJson<'a> body =
//    let rules = new JsonSerializerSettings ()
//    rules.MissingMemberHandling <- MissingMemberHandling.Error
//    try
//        let value = JsonConvert.DeserializeObject<'a> (body,rules)
//        if box value = null then Failure <| sprintf "Failed to deserialize %s" body
//        else Success value
//    with _ -> Failure <| sprintf "Failed to deserialize %s" body

let getEpoch = int (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds

let getAction url =
    let m = Regex.Match (url, @"/(\w*)/?$")
    if m.Success then
        Success m.Groups.[1].Value
    else
        Failure <| sprintf "Failed to get action in %s" url

let trimUrl url =
    let m = Regex.Match (url, @"(.+)/(\w+)/?$")
    let actions = ["executed"; "pending"; "included"; "executable"; "preconditions"; "lock"]
    if m.Success then
        match List.tryFind (fun state -> state = m.Groups.[2].Value) actions with
        | Some n -> Success m.Groups.[1].Value
        | None -> Success url
    else
        Failure <| sprintf "Failed to trim url: %s" url

let isExecutable (event: Event) =
    event.included && event.lock.IsNone && List.forall (fun _ -> false) <| event.preconditions

let authRole authLocation (token:string) (username:string) (eventRoles:string list) =
    let client = new System.Net.WebClient ()
    printf "%s" "adding headers"
    client.Headers.Add ("Token", token)
    printf "%A" (client.Headers.AllKeys)
    try
        let rolesJson =
            client.DownloadString (sprintf "%s/users/%s/roles" authLocation username)
        printf "%A" rolesJson
        let roles = JsonConvert.DeserializeObject<string list> rolesJson
        Success roles
    with e -> Failure e

let serverAuth authLocation (token:string) (username:string) (eventRoles:string list) =
    match authRole authLocation token username eventRoles with
    | Failure _ -> false
    | Success userRoles ->
        let role = userRoles.Head
        role = ""
        || role = "system-role"
        || List.isEmpty eventRoles
        || List.exists (fun elem -> List.exists (fun elem2 -> elem = elem2) userRoles) eventRoles

let tryGetSimple (events: ConcurrentDictionary<string,Event>) id =
    if events.ContainsKey id then Success events.[id]
    else Failure (404, sprintf "Event %s not found" id)

let tryGetEvent (events:ConcurrentDictionary<string,Event>) id accessor =
    let rec helper maxTimeout current id =
        if current > maxTimeout then Failure (503,"Timeout")
        else
            match tryGetSimple events id with
            | Success event when event.lock.IsNone || event.lock.Value = accessor -> Success event
            | Failure msg -> Failure msg
            | Success _ -> //event exists but cannot access right now
                System.Threading.Thread.Sleep 100
                helper maxTimeout (current+200) id
    helper 5000 0 id

let getEvent (url:string) (events:ConcurrentDictionary<string,Event>) =
    match trimUrl url with
    | Failure msg -> Failure (400, msg)
    | Success id ->
        match tryGetEvent events id "" with
        | Failure response -> Failure response
        | Success event ->
            match getAction url with
            | Failure _ -> Success (200, JsonConvert.SerializeObject <| event.setExecutable (isExecutable event))
            | Success action ->
                match action with
                | "executed" -> Success (200,string event.executed)
                | "included" -> Success (200,string event.included)
                | "pending" -> Success (200,string event.pending)
                | "executable" -> Success (200, string (isExecutable event))
                | _ -> Success (200, JsonConvert.SerializeObject <| event.setExecutable (isExecutable event))

let getEventCount (events:ConcurrentDictionary<string,Event>) =
    if events.Count >= 0 then (200,string events.Count)
    else (500, "")

//should likely be some restrictions on post, so you cannot post an event, which will distort the workflow
let postEvent (url:string) (body:string) (events:ConcurrentDictionary<string,Event>) =
    printfn "Received post request from %s" url
    if not (events.ContainsKey url) then
        printfn "post request url %s does not exist. Is valid for post" url
        match tryParseJson<Event> body with
        | Failure _ ->
            printfn "Failed to deserialise post to url %s" url 
            Failure (400, "Failed to deserialize json as Event type")
        | Success event ->
            printfn "received event %s" event.id
            events.[event.id] <- event.setCreated getEpoch
            Success (201, JsonConvert.SerializeObject event)
    else
        printfn "post event %s does already exist" url
        Failure (409, "Event already exists")

let sendPut (client: WebClient) attribute json id =
    try
        printfn "Sending put to %s" id
        let res = client.UploadString(sprintf "%s/%s" id attribute,"PUT", json)
        //printfn "%s responded with %s" id res
        true
    with
    _ as excep ->
        printfn "%s: error when communicating: %A" id excep
        false

let updateRelated (relation: string list) (attribute: string) (value: bool) =
    let json = JsonConvert.SerializeObject <| { username = ""; value = value; token = ""}
    let helper = sendPut (new WebClient ()) attribute json
    List.forall helper relation |> ignore
    ()

let updateRelatedWithRole (relation: string list) (attribute: string) (value: bool) (token: string) (role: string) =
    let json = JsonConvert.SerializeObject <| { username = role; value = value; token = token}
    let helper = sendPut (new WebClient ()) attribute json
    List.forall helper relation |> ignore
    ()


let updatePreconditions (relations: string list) (myName: string) (shouldAdd: bool) =
    printfn "%s: updating preconds -> %A" myName relations
    let data = {
        role = ""
        name = myName
        shouldAdd = shouldAdd
        }
    let json = JsonConvert.SerializeObject data
    let helper = sendPut (new WebClient ()) "preconditions" json
    List.forall helper relations |> ignore
    ()

let listContains item list =
    List.exists (fun elem -> elem = item) list

//            let req = WebRequest.Create (Uri target) :?> HttpWebRequest
//            req.Method <- "PUT"
//            req.ContentType <- "application/json"
//            req.ContentLength <- int64 <| String.length json
//            let dataStream = req.GetRequestStream ()
//            dataStream.w
//            use! resp = req.AsyncGetResponse ()
//            use stream = resp.GetResponseStream ()
//            use reader = new StreamReader (stream)
//            let contents = reader.ReadToEnd ()
//            return Success target

let sendLockMsg json (target: string) =
//    try 
//        let client = new WebClient ()
//        let js = JsonConvert.SerializeObject json
//        let res = client.UploadString (target,"PUT",js)
//        Success target
    async {
        try 
            let req = WebRequest.Create target :?> HttpWebRequest
            req.ContentType <- "application/json"
            req.Method <- "PUT"
            let js = JsonConvert.SerializeObject json
            let data = Encoding.ASCII.GetBytes js
            req.ContentLength <- int64 data.Length
            printf "sending request to %s" target
            let stream = req.GetRequestStream ()
            let! thing = stream.AsyncWrite (data, 0, data.Length)
            let! thing2 = Async.AwaitIAsyncResult <| stream.FlushAsync ()
            stream.Close ()
            stream.Dispose ()
            use! res = req.AsyncGetResponse ()
            printf "received response from %s" target
            use httpRes = res :?> HttpWebResponse
            let statusCode = httpRes.StatusCode
            httpRes.Close ()
            httpRes.Dispose ()
            if statusCode = HttpStatusCode.OK then return Success target
            else return Failure target
//            use stream = res.GetResponseStream ()
//            use reader = new StreamReader (stream)
//            let contents = reader.ReadToEnd ()
//            printfn "Lock retrieved"
//            return Success target
//            printfn "Retrieving lock..." 
//            let! req = Http.AsyncRequestString (target, httpMethod = "PUT", headers = [ ("Content-Type", "application/json") ], body = TextRequest (JsonConvert.SerializeObject json))
//            printfn "Response received"
//            return Success target
        with fail ->
            printfn "%A" fail 
            printfn "Response received"
            return Failure target
       }

let proceedIfUnlocked (event: Event) (accessor: string) =
    match event.lock with
    | None -> Success event
    | Some id when id = accessor -> Success event
    | _ -> Failure (401, sprintf "%s did not set the lock, so it cannot update" accessor)

let updateEvent (events: ConcurrentDictionary<string,Event>) (id: string) (accessor: string) (func: Event -> Result<'a,'b>) : Result<'a,'b> =
    printfn "Trying to access %s for %s" id accessor
    //lock (id) (fun () ->
    match tryGetSimple events id with
    | Failure msg -> Failure msg
    | Success event ->
        match proceedIfUnlocked event accessor with
        | Failure msg -> Failure msg
        | Success event -> func event
    //)

let getDependecies (event: Event) =
    List.ofSeq <| Set.ofList (event.conditions @ event.exclusions @ event.inclusions)
//    let set = Set.ofList <| event.conditions @ event.exclusions @ event.inclusions
//    if set.Contains event.id then List.ofSeq <| set.Remove event.id
//    else List.ofSeq set

//if fails, will return the items it managed to fold over
//if success, will return all items
let folder func set =
    let rec helper acc = function
        | [] -> Success acc
        | head::tail ->
            match func head with
            | true -> helper (head :: acc) tail
            | false -> Failure acc
    helper [] set

let parseResults ls =
    if List.exists (fun elem ->
        match elem with
        | Success _ -> false
        | Failure _ -> true) ls then

        let parser acc = function
            | Success id -> id :: acc
            | _ as fail -> acc
        Failure <| List.fold parser [] ls
    else
        Success ()
//    let parser acc = function
//        | Failure id -> id :: acc
//        | _ -> acc
//    match List.fold parser [] ls with
//    | [] ->
//        Success <| List.fold (fun acc elem ->
//            match elem with
//            | Failure _ -> acc
//            | Success id -> id :: acc) [] ls
//    | res -> Failure res

let sendToID sendFunc id =
    try
        match sendFunc id with
        | true -> Success id
        | false -> Failure id
    with e -> Failure id

//will attempt to send lock message to all receipients
//if lock of all fails, then will try to unlock the wrongfully locked ones
let attemptLocking (lockMsg: LOCK_DTO) receipients =
//    let results = List.map (fun target -> sendLockMsg lockMsg (sprintf "%s/lock" target)) receipients
    let results = List.ofArray (Async.RunSynchronously <| Async.Parallel [for target in receipients -> sendLockMsg lockMsg (sprintf "%s/lock" target) ])
    match parseResults results with
    | Success _ -> Success (200, "Successfully locked all events requested")
    | Failure actuallyLocked when lockMsg.value = true ->
        printfn "Failed to lock this: %A" <| List.filter (fun elem -> match elem with
            | Success _ -> false
            | Failure _ -> true) results//  .[actuallyLocked.Length]
        let unlockMsg = JsonConvert.SerializeObject { id = lockMsg.id; value = false; shouldPropagateToPrecond = lockMsg.shouldPropagateToPrecond }
//        let newResults = List.map (fun target -> sendLockMsg unlockMsg (sprintf "%s/lock" target)) actuallyLocked
        let newResults = List.ofArray (Async.RunSynchronously <| Async.Parallel [for target in actuallyLocked -> sendLockMsg unlockMsg target ])
        match parseResults newResults with
        | Success _ -> Failure (503, "Failed to lock all events requested, locks aborted. Please try again later")
        | Failure msg -> Failure (500, "Failed to lock all events, but locked some and then failed to unlock them again. Inconsistent state!!!")
    | Failure actuallyUnlocked -> Failure (500, "Failed to unlock all")

let tryExecute log authenticate (events: ConcurrentDictionary<string,Event>) (event: Event) (req: PUT_DTO) =
    match authenticate req.token req.username event.roles with
    | false -> Failure (401, "Unauthorized role")
    | true ->
        match isExecutable event with
        | false -> Failure (405, "Event cannot be executed at this time")
        | true ->
            let dependencies = getDependecies event
            printfn "exec event should lock: %A" dependencies
            let lockMsg = { id = event.id; value = true; shouldPropagateToPrecond = true }
            match attemptLocking lockMsg dependencies with
            | Failure msg as fail -> fail
            | Success _ ->
                let newEvent = event.setState true event.included false
                events.[event.id] <- newEvent
                updateRelatedWithRole event.inclusions "included" true req.token event.id
                updateRelatedWithRole event.exclusions "included" false req.token event.id
                updatePreconditions event.conditions event.id false
                events.[newEvent.id] <- events.[newEvent.id].setLock None
                let unlockMsg = { id = event.id; value = false; shouldPropagateToPrecond = true }
                match attemptLocking unlockMsg dependencies with
                | Failure _ as fail -> fail
                | Success _ ->
                    let rec getLastNumber acc (s: string) = 
                        let char = s.Chars (s.Length-1)
                        if Char.IsDigit char then
                            getLastNumber (string char + acc) (s.Remove (s.Length-1))
                        else 
                            acc
                    log (getLastNumber "" newEvent.id) "executed" 
                    Success (200, "Executed successfully")



let putEvent log authenticate (url:string) (body:string) (events:ConcurrentDictionary<string,Event>) =
    printfn "request to %s received" url
    match trimUrl url with
    | Failure msg -> Failure (400,msg)
    | Success id ->
        match getAction url with
        | Failure msg -> Failure (400,msg)
        | Success action ->
            match tryParseJson<PRECONDITION_DTO> body with
            | Success preCod when action = "preconditions" ->
                updateEvent events id preCod.name (fun event ->
                    printfn "%s: attempting to update preconditions for %s" event.id preCod.name
                    if preCod.shouldAdd then
                        if listContains preCod.name event.preconditions then Success(200,"")
                        else
                            events.[event.id] <- event.setPreconditions (preCod.name :: event.preconditions)
                            Success (200,"")
                    else
                        if listContains preCod.name event.preconditions then
                            let newPre = List.fold (fun acc item ->
                                if item = preCod.name then acc
                                else item :: acc) [] event.preconditions
                            let newEvent = event.setPreconditions newPre
                            events.[event.id] <- newEvent
                            Success (200, "")
                        else
                            Success (200, "")
                 )
            | Failure _ ->
                match tryParseJson<PUT_DTO> body with
                | Success putDto ->
                    match (action, putDto.value) with
                    | "executed", _ ->
                        updateEvent events id putDto.username (fun event ->
                            if putDto.value = false then Failure (400, "Cannot unexecute an event")
                            else
                                match tryExecute log authenticate events event putDto with
                                | Failure msg -> Failure msg
                                | Success msg -> Success msg
                            )
                    | "included", bool ->
                        updateEvent events id putDto.username (fun event ->
                            if bool then
                                if not event.executed then
                                    updatePreconditions event.conditions event.id true
                            else
                                updatePreconditions event.conditions event.id false
                            events.[event.id] <- event.setIncluded bool
                            Success (200,"")
                            )
                    | "pending", bool ->
                        updateEvent events id putDto.username (fun e ->
                            events.[e.id] <- e.setPending bool
                            Success (200,"")
                            )
                    | _ -> Failure (400, "Unknown action: " + action)
                | Failure msg ->
                    match tryParseJson<LOCK_DTO> body with
                    | Failure msg -> Failure (400, msg)
                    | Success dto ->
                        match action.ToLower () with
                        | "lock" ->
                            printfn "%s: lock action received from %s" id dto.id
                            updateEvent events id dto.id (fun e ->
                                if dto.value then
                                    events.[e.id] <- e.setLock <| Some dto.id
                                    if dto.shouldPropagateToPrecond then
                                        let lockMsg = { id = dto.id; value = true; shouldPropagateToPrecond = false }
                                        let receipients = List.filter (fun elem -> elem <> e.id) <| e.inclusions @ e.exclusions
                                        printfn "%s should lock: %A" e.id receipients
                                        match attemptLocking lockMsg receipients with
                                        | Failure _ as fail -> 
                                            printfn "%s: Failed to lock my neighbours for %s" id dto.id
                                            fail
                                        | Success _ ->
                                            printfn "%s: lock granted to %s" id dto.id
                                            Success (200,"")
                                    else
                                        printfn "%s: Lock granted to %s" id dto.id
                                        Success (200, "")
                                else
                                    printfn "%s: unlock requested from %s" id dto.id
                                    if dto.shouldPropagateToPrecond then
                                        let lockMsg = { id = dto.id; value = false; shouldPropagateToPrecond = false }
                                        let receipients = List.filter (fun elem -> elem <> e.id) <| e.inclusions @ e.exclusions
                                        printfn "%s should lock: %A" e.id receipients
                                        match attemptLocking lockMsg receipients with
                                        | Failure _ as fail -> 
                                            printfn "%s: failed to unlock all my neightbours for %s" id dto.id
                                            fail
                                        | Success _ ->
                                            events.[e.id] <- e.setLock None
                                            printfn "%s: successfully unlocked for %s" id dto.id
                                            Success (200,"")
                                    else
                                        events.[e.id] <- e.setLock None
                                        printfn "%s: successfully unlocked for %s" id dto.id
                                        Success (200, "")
                            )
                        | _ -> Failure (400, sprintf "Unknown action: %s" action)
            | _ -> Failure (400, "Failed to deserialize json")

let serverDeployer url myUrl =
    tryUpload (sprintf "%s/subscribe" url) "POST" myUrl

let serverLog url id action =
    let client = new WebClient()
    try 
        client.UploadString (sprintf "%s/events/%s/%s" url id action,"POST","") |> ignore
        printf "logged action %s for id %s to log service at %s" action id url
    with
        e ->
            printf "Failed to log action %s for id %s to log service at %s" action id url
            printf "%A" e

type Host (port0:int, url0:string, logger, authenticate, subscribeToDeployer) =
    inherit Server(port0)
    let events = new ConcurrentDictionary<string,Event>()
    let rec subscribe () =
        match subscribeToDeployer url0 with
        | Failure (code,reason) ->
            printfn "%d: %s" code reason
            ()
        | Success (res: string) -> ()
    do
        subscribe () |> ignore

    member this.RespondWithResult (res: HttpListenerResponse) = function
        | Success tuple -> this.RespondWithTuple tuple res
        | Failure (code,reason) -> this.RespondWithReason code reason "" res

    override this.Handle (req:HttpListenerRequest) (resp:HttpListenerResponse) = async {
        printfn "request to %s" req.Url.AbsoluteUri
        match req.HttpMethod with
        | "GET" ->
            if req.Url.AbsoluteUri.EndsWith "eventCount" then
                this.RespondWithTuple (getEventCount events) resp
            else
                this.RespondWithResult resp (getEvent req.Url.AbsoluteUri events)
        | "POST" ->
            this.RespondWithResult resp (postEvent req.Url.AbsoluteUri (this.GetBody req) events)
        | "PUT" ->
            this.RespondWithResult resp (putEvent logger authenticate req.Url.AbsoluteUri (this.GetBody req) events)
        | "OPTIONS" ->
            this.Respond 200 "Success" resp
        | _ ->
            this.Respond 400 "Bad request" resp
    }