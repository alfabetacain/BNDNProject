# XML structure

## Relevant nodes and attributes

Relevant attributes/nodes for our parser. Note that this does not include roles (possible addition later on).

- /dcrgraph
    - @title: Name of workflow
- /dcrgraph/specification/resources/labels/label[i]: Label of event i
- /dcrgraph/specification/resources/labelMappings/labelMapping
    - @eventId: Identifier of event
- /dcrgraph/specification/resources/labelMappings/labelMapping
    - @labelId: Label of event
- /dcrgraph/specification/constraints/conditions/condition
    - @sourceId: Identifier of constraining event
    - @targetId: Identifier of constrained event
- /dcrgraph/specification/constraints/responses/response
    - @sourceId: Identifier of ordering event
    - @targetId: Identifier of responding event
- /dcrgraph/specification/constraints/excludes/exclude
    - @sourceId: Identifier of excluding event
    - @targetId: Identifier of excluded event
- /dcrgraph/specification/constraints/includes/include
    - @sourceId: Identifier of including event
    - @targetId: Identifier of included event
- /dcrgraph/runtime/marking/executed/event
    - @id: Identifier of executed event
- /dcrgraph/runtime/marking/included/event
    - @id: Identifier of included event
- /dcrgraph/runtime/marking/pendingResponses/event
    - @id: Identifier of pending event

## Things we will have to consider

- The parser doesn't know the urls/guids of events (the nature of this implementation is permanent). I suggest giving them zero indexed numbers and assigning real urls/guids later on.

## Things I don't understand

- The concept of levels (and filter levels)
- The concept of scopes (what are they used for)

## Fucked up things

- Sometimes the identifier of an event is of the form "Event #", while other times it is of the form "Activity #". Seems to be some left over legacy stuff. Doesn't happen with newly created workflows. The current standard form is "Activity #".