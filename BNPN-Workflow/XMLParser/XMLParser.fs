module XMLParser

open System.Xml

open Items

// Helper functions for XmlDocument

let xpSeq (doc:XmlDocument) query : list<XmlNode> =
    doc.SelectNodes query |> Seq.cast<XmlNode> |> List.ofSeq

// Start of functions that get things from a doc

let getTitle (doc:XmlDocument) =
    (doc.SelectSingleNode "/dcrgraph/@title").Value

let getIds (doc:XmlDocument) : list<string> =
    (xpSeq doc "//resources/events/event/@id")
    |> List.map (fun node -> node.Value)

//Get list of all present roles in complete XMLdoc
let getAllRoles (doc:XmlDocument) : list<string> =
    (xpSeq doc "//resources/custom/roles/role")
    |> List.map (fun node -> node.InnerText)

let getName (doc:XmlDocument) id =
    let query = sprintf "//labelMapping[@eventId='%s']/@labelId" id
    System.Text.RegularExpressions.Regex.Replace ((doc.SelectSingleNode query).Value, @"\r\n?|\n", " ") //so that labels do not contain line breaks

let getBool (doc:XmlDocument) id query =
    let query = sprintf query id
    doc.SelectSingleNode query <> null

let getExecuted doc id = getBool doc id "//executed/event[@id='%s']"
let getIncluded doc id = getBool doc id "//included/event[@id='%s']"
let getPending doc id = getBool doc id "//pendingResponses/event[@id='%s']"

let getRelations doc id query (attribute:string) =
    (xpSeq doc (sprintf query id))
    |> List.map (fun node -> node.Attributes.ItemOf(attribute).Value)

let getConditions doc id = getRelations doc id "//condition[@sourceId='%s']" "targetId";
let getPreconditions doc id = getRelations doc id "//condition[@targetId='%s']" "sourceId";
let getResponses doc id = getRelations doc id "//response[@sourceId='%s']" "targetId";
let getExclusions doc id = getRelations doc id "//exclude[@sourceId='%s']" "targetId";
let getInclusions doc id = getRelations doc id "//include[@sourceId='%s']" "targetId";

let getRoles doc id query =
    (xpSeq doc (sprintf query id))
    |> List.map (fun node -> node.InnerText)

    //This is probably not the best way to check..
let getRole doc id = if not (getRoles doc id "//events/event[@id='%s']/custom/roles/role" = [""]) 
                        then getRoles doc id "//events/event[@id='%s']/custom/roles/role" 
                        else [];

let getResources doc id = [];

let getEvent doc id = {
    id = id
    label = getName doc id
    executed = getExecuted doc id
    included = getIncluded doc id
    pending = getPending doc id
    preconditions = getPreconditions doc id
    conditions = getConditions doc id
    responses = getResponses doc id
    exclusions = getExclusions doc id
    inclusions = getInclusions doc id
    resources = getResources doc id
    created = -1
    modified = -1
    roles = getRole doc id
    executable = false
    lock = None
}

let getWorkflow doc : Workflow =
    let ids = getIds doc
    {
        id = ""
        title = getTitle doc
        roles = getAllRoles doc
        events = List.fold (fun map id -> Map.add id (getEvent doc id) map) Map.empty ids
    }
