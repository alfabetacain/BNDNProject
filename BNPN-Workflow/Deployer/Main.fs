open System

open Deployer

[<EntryPoint>]
let main args =
    let defaults = [
        ("PORT", "8002");
        ("HIPPO", "false");
        ("STORAGE", "http://ec2-52-28-69-195.eu-central-1.compute.amazonaws.com:8000");
    ]
    let getEnv key value =
        match Environment.GetEnvironmentVariable key with
        | null -> value
        | env -> env
    let input = defaults |> Map.ofList |> Map.map getEnv
    let deployer = new Deployer(Int32.Parse input.["PORT"], Boolean.Parse input.["HIPPO"], input.["STORAGE"])
    deployer.Listen
    printfn "Press return to exit..."
    Console.ReadLine() |> ignore
    0