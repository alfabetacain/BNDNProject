module Deployer

open System.Net
open System.Text
open System.IO
open System.Xml
open System.Collections.Concurrent
open System.Text.RegularExpressions

open Newtonsoft.Json

open Items
open XMLParser
open Server
open Lib

let urlifyWorkflow urls (workflow:Workflow) : Workflow =
    let ids = workflow.events |> Map.toList |> List.map fst
    let map = List.fold2 (fun map id url -> Map.add id url map) Map.empty ids urls
    let urlifyList list = List.map (fun id -> map.[id]) list
    let urlifyEvent (event:Event) = {
        id = map.[event.id]
        label = event.label
        executed = event.executed
        included = event.included
        pending = event.pending
        preconditions = urlifyList event.preconditions
        conditions = urlifyList event.conditions
        responses = urlifyList event.responses
        exclusions = urlifyList event.exclusions
        inclusions = urlifyList event.inclusions
        resources = urlifyList event.resources
        created = event.created
        modified = event.modified
        roles = event.roles
        executable = false
        lock = None
    }
    let folder wf id event =
        Map.add map.[id] (urlifyEvent event) wf
    {workflow with events = Map.fold folder Map.empty workflow.events}

let upload (address: string) protocol data =
    use client = new WebClient ()
    try
        let res = client.UploadString (address,protocol,data)
        Success res
    with
    | :? WebException as ex when (ex.Response :? HttpWebResponse) ->
        use response = ex.Response :?> HttpWebResponse
        Failure <| sprintf "ERROR -> %d: %s" (int response.StatusCode) response.StatusDescription
    | excep -> Failure <| excep.ToString ()

let postWorkflow (wf:Workflow) =
    let postEvent id event =
        let jsonified = JsonConvert.SerializeObject event
        let response = upload id "POST" jsonified
        match response with
        | Success s ->
            ()
        | Failure s ->
            printfn "%s" s
        ()
    Map.iter postEvent wf.events
let tryGet (url:string) =
    try
        let webClient = new WebClient()
        Success (webClient.DownloadString(url))
    with
        :? WebException as ex when (ex.Response :? HttpWebResponse) ->
        let response = ex.Response :?> HttpWebResponse
        Failure <| (int response.StatusCode,response.StatusDescription)
        |excep -> Failure <| (500, string excep)


type Deployer (port0:int, hippo0:bool, storage0:string) =
    inherit Server(port0)
    let storage = storage0
    let mutable hostUrls = []
    let mutable guid = 0
    let mutable x = 0
    let mutable hippo = hippo0
    let workflows = new ConcurrentDictionary<string,WorkflowDTO>()

    member this.GetUrl id =
        let url = sprintf "%s/events/%s" this.GetBaseUrl id
        url

    member this.Subscribe (url:string) =
        printfn "%s subscribed" url
        hostUrls <- url::hostUrls
        (200,"Success")

    member this.GetBaseUrl =
        if (not hippo) then
            let client = new System.Net.WebClient ()
            let list = [for url in hostUrls -> ((client.DownloadString (url+"/eventCount")),url)]
            let minCountUrl = List.minBy fst list |> snd
            minCountUrl
        else
            if x+1 = (List.length hostUrls) then x <- 0
            let baseUrl = hostUrls.[x]
            x <- x+1
            baseUrl

    // Encapsulated this for test purposes
    member this.HandlePostWF body =
        match tryParseJson<DcrIdDTO> body with
            |Failure msg -> (400, msg)
            |Success dto ->
            match tryGet (sprintf "%s/dcrs/%d" storage dto.DCRID) with
                |Failure fail -> fail
                |Success dcrJson ->
                match tryParseJson<DcrDTO> dcrJson with
                    |Failure fail -> (400,fail)
                    |Success dcrDTO ->
                    let doc = new XmlDocument()
                    doc.LoadXml dcrDTO.XML
                    let wf = XMLParser.getWorkflow doc
                    match tryUpload (storage+"/workflows/") "POST" body with
                        |Failure fail -> fail
                        |Success wfId ->
                        let uploadEvent event =
                            let dto = { Label = event.label; WorkflowID = int wfId }
                            match tryUpload (storage+"/events/") "POST" (JsonConvert.SerializeObject dto) with
                            |Success id -> id
                            |Failure fail -> failwith "We could not host the events"

                        let ids = Map.fold (fun acc key elem -> uploadEvent elem :: acc) [] wf.events
                        let urls = List.map this.GetUrl ids
                        let id = string wfId
                        let urlifiedWf = {urlifyWorkflow urls wf with id = id}
                        postWorkflow urlifiedWf
                        let wfDto = {
                            id = urlifiedWf.id
                            title = urlifiedWf.title
                            roles = urlifiedWf.roles
                            events = urls
                        }
                        workflows.[id] <- wfDto
                        (200,JsonConvert.SerializeObject wfDto)

    member this.HandlePost req =
        let body = this.GetBody req
        this.HandlePostWF body

    member this.HandleGet url =
        let m = Regex.Match (url, @"/(\w+)/?$")
        if m.Success then
            match m.Groups.[1].Value with
            | "workflows" ->
                (200,JsonConvert.SerializeObject workflows)
            |  id ->
                match this.HandleGetId(id) with
                | Success urlies ->
                    (200, JsonConvert.SerializeObject urlies)
                | Failure msg ->
                    (404, "Not found")
        else
            (400, "Bad request")

    member this.HandleGetId id =
        if workflows.ContainsKey id then
            Success workflows.[id]
        else
            Failure "No workflow with the given id"

    override this.Handle (req:HttpListenerRequest) (resp:HttpListenerResponse) = async {
        // printfn "[Deployer] Handling request"
        match req.HttpMethod with
        | "POST" ->
            if req.Url.AbsoluteUri.EndsWith "subscribe" then
                this.RespondWithTuple (this.Subscribe <| this.GetBody req) resp
            else
                this.RespondWithTuple (this.HandlePost req) resp
        | "GET" ->
            this.RespondWithTuple (this.HandleGet req.Url.AbsoluteUri) resp
        | "OPTIONS" ->
            this.Respond 200 "Success" resp
        | _ ->
            this.Respond 400 "Bad request" resp
    }