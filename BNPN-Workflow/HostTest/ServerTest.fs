﻿module ServerTest

open NUnit.Framework
open FsUnit
open Items
open Host
open Newtonsoft.Json
open System.Collections.Concurrent
open System.Net
open Auth

let storageUrl = "http://ec2-52-28-69-195.eu-central-1.compute.amazonaws.com:8000"
let authUrl = "http://localhost:8001"
let deployerUrl = "http://localhost:8002"
let host0Url = "http://localhost:8010"
let host1Url = "http://localhost:8011"
let host2Url = "http://localhost:8012"

let isSuccess = function
    | Success _ -> true
    | Failure _ -> false

let alwaysTrueAuth _ _ _ = true
let alwaysSubscribeSuccessfully _ = Success ""
let alwaysLogSuccess _ _ = ()

[<Test>]
let ``getState when called with "http://www.google.com/events/42/executed" returns Success("executed")`` () =
    //arrange
    let expected = Success("executed")
    //act
    let result = getAction "http://www.google.com/events/42/executed"
    //assert
    Assert.AreEqual(expected, result)

[<Test>]
let ``parseUrl when called with "http://www.google.com/events/42/executed" returns Success("http://www.google.com/events/42")`` () =
    //arrange
    let expected = Success("http://www.google.com/events/42")
    //act
    let result = trimUrl "http://www.google.com/events/42/executed"
    //assert
    Assert.AreEqual(expected, result)

//prepare for getEvent
let event ={
    id = "http://www.google.com/events/42"
    label = "Some Event"
    executed = true
    pending = true
    included = true
    preconditions = []
    conditions = []
    responses = []
    exclusions = []
    inclusions = []
    resources = []
    created = -1
    modified = -1
    roles = []
    executable = false
    lock = None
}

[<Test>]
let ``getEvent when called with "http://www.google.com/events/42" returns 200`` () =
    //arrange
    let workflow = new ConcurrentDictionary<string,Event>() //Map.empty.Add("http://www.google.com/events/42",event)
    workflow.[event.id] <- event
    //act
    let result = (getEvent "http://www.google.com/events/42" workflow)
    //assert
    match result with
    | Failure msg -> Assert.Fail <| string msg
    | Success (code,_) -> Assert.AreEqual (200, code)

[<Test>]
let ``getEvent when called with "http://www.google.com/events/42" returns 404`` () =
    //arrange
    let workflow = new ConcurrentDictionary<string,Event>()
    //act
    let result = (getEvent "http://www.google.com/events/42" workflow)
    //assert
    match result with
    | Failure (code,_) -> Assert.AreEqual (404, code)
    | _ -> Assert.Fail "should not be able to retrieve an event which is not there"

[<Test>]
let ``getEvent when called with "http://www.google.com/events/42/executed" returns 200`` () =
    //arrange
    let workflow = new ConcurrentDictionary<string,Event>()
    workflow.[event.id] <- event
    //act
    let result = (getEvent "http://www.google.com/events/42/executed" workflow)
    //assert
    match result with
    | Failure msg -> Assert.Fail <| string msg
    | Success (code,_) -> Assert.AreEqual (200,code)

[<Test>]
let ``getEvent when called with "http://www.google.com/events/42/executed/" returns 200`` () =
    //arrange
    let workflow = new ConcurrentDictionary<string,Event>()
    workflow.[event.id] <- event
    //act
    let result = (getEvent "http://www.google.com/events/42/executed/" workflow)
    //assert
    match result with
    | Success (code,_) -> Assert.AreEqual (200, code)
    | Failure msg -> Assert.Fail <| string msg

let getBool s =
    try
        System.Boolean.Parse s
    with _ -> false

[<Test>]
let ``getEvent when called with "http://www.google.com/events/42/executed" returns true`` () =
    //arrange
    let workflow = new ConcurrentDictionary<string,Event>()
    workflow.[event.id] <- event
    //act
    let r = getEvent "http://www.google.com/events/42/executed" workflow
    match r with
    | Failure _ -> Assert.Fail ()
    | Success _ -> ()

[<Test>]
let ``getEvent when called with "" returns 400`` () =
    //arrange
    let workflow = new ConcurrentDictionary<string,Event>()
    //act
    let result = (getEvent "" workflow)
    //assert
    match result with
    | Failure (code,_) -> Assert.AreEqual (400, code)
    | _ -> Assert.Fail "get event should have failed"

[<Test>]
let ``putEvent when called with "http://www.google.com/events/42" returns 400`` () =
    //arrange
    let workflow = new ConcurrentDictionary<string,Event>()
    workflow.[event.id] <- event
    let putter = {
        username = ""
        value = true
        token = "system-token"
    }
    //act
    let result = (putEvent alwaysLogSuccess alwaysTrueAuth "http://www.google.com/events/42" (JsonConvert.SerializeObject putter) workflow)
    //assert
    match result with
    | Failure msg -> Assert.AreEqual(400, fst msg)
    | _ -> Assert.Fail ()

[<Test>]
let ``putEvent when called with "http://localhost:8010/events/43/execute" 404`` () =
    //arrange
    let workflow = new ConcurrentDictionary<string,Event>()
    workflow.[event.id] <- event
    let putter = {
        username = "system"
        value = true
        token = "system-token"
    }
    //act
    let result = (putEvent alwaysLogSuccess alwaysTrueAuth "http://localhost:8010/events/43/executed" (JsonConvert.SerializeObject putter) workflow)
    //assert
    match result with
    | Failure msg ->
        Assert.AreEqual(404, fst msg)
    | _ -> Assert.Fail ()

[<Test>]
let ``putEvent when called with "", false returns 400`` () =
    //arrange
    let workflow = new ConcurrentDictionary<string,Event>()
    workflow.[event.id] <- event
    let putter = {
        username = ""
        value = false
        token = "system-token"
    }
    //act
    let result = (putEvent alwaysLogSuccess alwaysTrueAuth "" "" workflow)
    //assert
    match result with
    | Failure msg -> Assert.AreEqual(400, fst msg)
    | _ -> Assert.Fail ()

[<Test>]
let ``postEvent when called with "http://www.google.com/events/42" returns 201`` () =
    //arrange
    let workflow = new ConcurrentDictionary<string,Event>()
    let serialized = JsonConvert.SerializeObject event
    //act
    let result = (postEvent "http://www.google.com/events/42" serialized workflow)
    //assert
    match result with
    | Success (code,_) -> Assert.AreEqual (201, code)
    | Failure msg -> Assert.Fail <| string msg

[<Test>]
let ``postEvent when called with "http://www.google.com/events/42", event dublicate returns 409`` () =
    //arrange
    let workflow = new ConcurrentDictionary<string,Event>()
    workflow.[event.id] <- event
    let serialized = JsonConvert.SerializeObject event
    //act
    let result = (postEvent "http://www.google.com/events/42" serialized workflow)
    //assert
    match result with
    | Failure (code,_) -> Assert.AreEqual (409, code)
    | _ -> Assert.Fail "WHo knows"

[<Test>]
let ``postEvent when called with "http://www.google.com/events/42", no event returns 400`` () =
    //arrange
    let workflow = new ConcurrentDictionary<string,Event>()
    //act
    let result = (postEvent "http://www.google.com/events/42" "" workflow)
    //assert
    match result with
    | Failure (code,_) -> Assert.AreEqual (400,code)
    | _ -> Assert.Fail "What?"

[<Test>]
let ``putEvent removes precondition`` () =
    //arrange
    let workflow = new ConcurrentDictionary<string,Event>()
    let event = {
        id = "http://www.google.com/events/43"
        label = "main event"
        executed = false
        included = true
        pending = false
        conditions = []
        responses = []
        exclusions = []
        inclusions = []
        preconditions = ["http://www.google.com/events/1"]
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = false
        lock = None
    }
    workflow.[event.id] <- event
    workflow.[event.id] <- event
    let addDTO = {
        role = ""
        name = "http://www.google.com/events/1"
        shouldAdd = false
        }
    let json = JsonConvert.SerializeObject addDTO
    //act
    let result = (putEvent alwaysLogSuccess alwaysTrueAuth "http://www.google.com/events/43/preconditions" json workflow)
    //assert
    match result with
    | Success msg -> Assert.AreEqual(200, fst msg)
    | _ -> Assert.Fail ()
    Assert.AreEqual([], workflow.[event.id].preconditions)

[<Test>]
let ``putEvent adds precondition`` () =
    //arrange
    let workflow = new ConcurrentDictionary<string,Event>()
    let event = {
        id = "http://www.google.com/events/43"
        label = "main event"
        executed = false
        included = true
        pending = false
        conditions = []
        responses = []
        exclusions = []
        inclusions = []
        preconditions = []
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = false
        lock = None
    }
    workflow.[event.id] <- event
    let addDTO = {
        role = ""
        name = "http://www.google.com/events/1"
        shouldAdd = true
        }
    let json = JsonConvert.SerializeObject addDTO
    //act
    let result = (putEvent alwaysLogSuccess alwaysTrueAuth "http://www.google.com/events/43/preconditions" json workflow)
    //assert
    match result with
    | Success msg -> Assert.AreEqual(200, fst msg)
    | _ -> Assert.Fail ()
    Assert.AreEqual(["http://www.google.com/events/1"], workflow.[event.id].preconditions)

[<Test>]
let ``executing an event removes precondition`` () =
    //arrange
    let workflow = new ConcurrentDictionary<string,Event>()
    let event1 = {
        id = "http://localhost:8010/events/1"
        label = "main event"
        executed = false
        included = true
        pending = false
        conditions = ["http://localhost:8010/events/2"]
        responses = []
        exclusions = []
        inclusions = []
        preconditions = []
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = false
        lock = None
    }
    let event2 = {
        id = "http://localhost:8010/events/2"
        label = "main event"
        executed = false
        included = true
        pending = false
        conditions = []
        responses = []
        exclusions = []
        inclusions = []
        preconditions = ["http://localhost:8010/events/1"]
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = false
        lock = None
    }
    let client = new WebClient ()
    let host = new Host (8010, host0Url, alwaysLogSuccess, alwaysTrueAuth, alwaysSubscribeSuccessfully)
    let listen = async {
        try
            host.Listen
        with
            _ -> ()
         }
    Async.Start listen
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event1)) |> ignore
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event2)) |> ignore
    let addDTO = {//("",true)
        username = "system"
        value = true
        token = "system-token"
        }
    //act
    let json = JsonConvert.SerializeObject addDTO
    client.UploadString ((sprintf "%s/%s" event1.id "executed"), "PUT", json) |> ignore
    //assert
    let data = client.DownloadString event2.id
    let event = JsonConvert.DeserializeObject<Event> data
    //printf "%A" event
    host.Stop ()
    Assert.AreEqual([], event.preconditions)


[<Test>]
let ``excluding an event removes precondition`` () =
    //arrange
    let workflow = new ConcurrentDictionary<string,Event>()
    let event1 = {
        id = "http://localhost:8010/events/1"
        label = "main event"
        executed = false
        included = true
        pending = false
        conditions = ["http://localhost:8010/events/2"]
        responses = []
        exclusions = []
        inclusions = []
        preconditions = []
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = false
        lock = None
    }
    let event2 = {
        id = "http://localhost:8010/events/2"
        label = "main event"
        executed = false
        included = true
        pending = false
        conditions = []
        responses = []
        exclusions = []
        inclusions = []
        preconditions = ["http://localhost:8010/events/1"]
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = false
        lock = None
    }
    let client = new WebClient ()
    let host = new Host (8010, host0Url, alwaysLogSuccess, alwaysTrueAuth, alwaysSubscribeSuccessfully)
    let listen = async {
        try
            host.Listen
        with
            _ -> ()
         }
    Async.Start listen
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event1)) |> ignore
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event2)) |> ignore
    let addDTO = {
        username = ""
        value = false
        token = "system-token"
        }//("",false)
    //act
    let json = JsonConvert.SerializeObject addDTO
    client.UploadString ((sprintf "%s/%s" event1.id "included"), "PUT", json) |> ignore
    //assert
    let data = client.DownloadString event2.id
    let event = JsonConvert.DeserializeObject<Event> data
    //printf "%A" event
    host.Stop ()
    Assert.AreEqual([], event.preconditions)

[<Test>]
let ``including an not executed event adds precondition`` () =
    //arrange
    let workflow = new ConcurrentDictionary<string,Event>()
    let event1 = {
        id = "http://localhost:8010/events/1"
        label = "main event"
        executed = false
        included = false
        pending = false
        conditions = ["http://localhost:8010/events/2"]
        responses = []
        exclusions = []
        inclusions = []
        preconditions = []
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = false
        lock = None
    }
    let event2 = {
        id = "http://localhost:8010/events/2"
        label = "main event"
        executed = false
        included = true
        pending = false
        conditions = []
        responses = []
        exclusions = []
        inclusions = []
        preconditions = []
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = false
        lock = None
    }
    let client = new WebClient ()
    let host = new Host (8010, host0Url, alwaysLogSuccess, alwaysTrueAuth, alwaysSubscribeSuccessfully)
    let listen = async {
        try
            host.Listen
        with
            _ -> ()
         }
    Async.Start listen
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event1)) |> ignore
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event2)) |> ignore
    let addDTO = {
        username = ""
        value = true
        token = "system-token"
        }//("",true)
    //act
    let json = JsonConvert.SerializeObject addDTO
    client.UploadString ((sprintf "%s/%s" event1.id "included"), "PUT", json) |> ignore
    //assert
    let data = client.DownloadString event2.id
    let event = JsonConvert.DeserializeObject<Event> data
    //printf "%A" event
    host.Stop ()
    Assert.AreEqual(["http://localhost:8010/events/1"], event.preconditions)

[<Test>]
let ``including an executed event does not add precondition`` () =
    //arrange
    let workflow = new ConcurrentDictionary<string,Event>()
    let event1 = {
        id = "http://localhost:8010/events/1"
        label = "main event"
        executed = true
        included = false
        pending = false
        conditions = ["http://localhost:8010/events/2"]
        responses = []
        exclusions = []
        inclusions = []
        preconditions = []
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = false
        lock = None
    }
    let event2 = {
        id = "http://localhost:8010/events/2"
        label = "main event"
        executed = false
        included = true
        pending = false
        conditions = []
        responses = []
        exclusions = []
        inclusions = []
        preconditions = []
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = false
        lock = None
    }
    let client = new WebClient ()
    let host = new Host (8010, host0Url, alwaysLogSuccess, alwaysTrueAuth, alwaysSubscribeSuccessfully)
    let listen = async {
        try
            host.Listen
        with
            _ -> ()
         }
    Async.Start listen
    let putDto = { username = "system"; value = true; token = "system-token" }
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event1)) |> ignore
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event2)) |> ignore
    //act
    let json = JsonConvert.SerializeObject putDto
    client.UploadString ((sprintf "%s/%s" event1.id "included"), "PUT", json) |> ignore
    //assert
    let data = client.DownloadString event2.id
    let event = JsonConvert.DeserializeObject<Event> data
    //printf "%A" event
    host.Stop ()
    Assert.AreEqual([], event.preconditions)

[<Test>]
let ``can execute with correct username, token and role`` () =
    //arrange
    let workflow = new ConcurrentDictionary<string,Event>()
    let event1 = {
        id = "http://localhost:8010/events/1"
        label = "main event"
        executed = false
        included = true
        pending = false
        conditions = []
        responses = []
        exclusions = []
        inclusions = []
        preconditions = []
        resources = []
        created = 0
        modified = 0
        roles = ["Specialist"]
        executable = false
        lock = None
    }
    let client = new WebClient ()
    let host = new Host (8010, host0Url, alwaysLogSuccess, alwaysTrueAuth, alwaysSubscribeSuccessfully)
    let listen = async {
        try
            host.Listen
        with
            _ -> ()
         }
    Async.Start listen
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event1)) |> ignore
    let addDTO = {
        username = "system"
        value = true
        token = "system-token"
        }//("doctor",true)
    //act
    let json = JsonConvert.SerializeObject addDTO
    try
        client.UploadString ((sprintf "%s/%s" event1.id "executed"), "PUT", json) |> ignore
        host.Stop ()
    with
        | _ ->
            host.Stop ()
            Assert.Fail ()

[<Test>]
let ``can execute an events which excludes itself`` () =
    let workflow = new ConcurrentDictionary<string,Event>()
    let event1 = {
        id = "http://localhost:8010/events/1"
        label = "main event"
        executed = false
        included = true
        pending = false
        conditions = []
        responses = []
        exclusions = ["http://localhost:8010/events/1"]
        inclusions = []
        preconditions = []
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = false
        lock = None
    }
    let client = new WebClient ()
    let host = new Host (8010, host0Url, alwaysLogSuccess, alwaysTrueAuth, alwaysSubscribeSuccessfully)
    let listen = async {
        try
            host.Listen
        with
            _ -> ()
         }
    Async.Start listen
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event1)) |> ignore
    let putDto = {
        username = "system"
        value = true
        token = "system-token"
        }
    //act
    let json = JsonConvert.SerializeObject putDto
    try
        client.UploadString ((sprintf "%s/%s" event1.id "executed"), "PUT", json) |> ignore
        let resultingEvent = JsonConvert.DeserializeObject<Event> (client.DownloadString "http://localhost:8010/events/1")
        host.Stop ()
        Assert.True resultingEvent.executed
        Assert.True resultingEvent.lock.IsNone
        Assert.False resultingEvent.executable
        Assert.False resultingEvent.included
    with
        | excep ->
            host.Stop ()
            printf "%A" excep
            Assert.Fail ()

[<Test>]
let ``cannot execute a locked event`` () =
    let workflow = new ConcurrentDictionary<string,Event>()
    let event1 = {
        id = "http://localhost:8010/events/1"
        label = "main event"
        executed = false
        included = true
        pending = false
        conditions = []
        responses = []
        exclusions = ["http://localhost:8010/events/1"]
        inclusions = []
        preconditions = []
        resources = []
        created = 0
        modified = 0
        roles = ["doctor"]
        executable = false
        lock = Some "ITU"
    }
    let client = new WebClient ()
    let host = new Host (8010, host0Url, alwaysLogSuccess, alwaysTrueAuth, alwaysSubscribeSuccessfully)
    let listen = async {
        try
            host.Listen
        with
            _ -> ()
         }
    Async.Start listen
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event1)) |> printf "%s"
    let putDto = {
        username = "doctor"
        value = true
        token = "Alien"
        }
    //act
    let json = JsonConvert.SerializeObject putDto
    try
        client.UploadString ((sprintf "%s/%s" event1.id "executed"), "PUT", json) |> printf "%s"
        let resultingEvent = JsonConvert.DeserializeObject<Event> (client.DownloadString "http://localhost:8010/events/1")
        host.Stop ()
        Assert.Fail "Should not be able to execute a locked event"
    with
        | :? WebException as webe ->
            host.Stop ()
            printf "%A" webe
            let resp = webe.Response :?> HttpWebResponse
            Assert.AreEqual(401, int resp.StatusCode)

[<Test>]
let ``cannot lock an already locked dependent event``() =
    let event1 = {
        id = "http://localhost:8010/events/1"
        label = "main event"
        executed = false
        included = true
        pending = false
        conditions = []
        responses = []
        exclusions = ["http://localhost:8010/events/2"]
        inclusions = []
        preconditions = []
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = false
        lock = None
    }
    let event2 = {
        id = "http://localhost:8010/events/2"
        label = "dependent event"
        executed = false
        included = true
        pending = false
        conditions = []
        responses = []
        exclusions = []
        inclusions = []
        preconditions = []
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = true
        lock = Some "Santa Claus"
    }
    let client = new WebClient ()
    let host = new Host (8010, host0Url, alwaysLogSuccess, alwaysTrueAuth, alwaysSubscribeSuccessfully)
    let listen = async {
        try
            host.Listen
        with
            _ -> ()
         }
    Async.Start listen
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event1)) |> ignore
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event2)) |> ignore
    let putDto = {
        username = "system"
        value = true
        token = "system-token"
        }
    //act
    let json = JsonConvert.SerializeObject putDto
    try
        client.UploadString ((sprintf "%s/%s" event1.id "executed"), "PUT", json) |> ignore
        let resultingEvent = JsonConvert.DeserializeObject<Event> (client.DownloadString event1.id)
        printf "%A" resultingEvent
        host.Stop ()
        Assert.Fail "Should not be able to execute a locked event"
    with
        | :? WebException as webe ->
            host.Stop ()
            let resp = webe.Response :?> HttpWebResponse
            printf "%A" resp.StatusDescription
            Assert.AreEqual(503, int resp.StatusCode)


[<Test>]
let ``can execute an event which must lock another event``() =
    let event1 = {
        id = "http://localhost:8010/events/1"
        label = "main event"
        executed = false
        included = true
        pending = false
        conditions = []
        responses = []
        exclusions = ["http://localhost:8010/events/2"]
        inclusions = []
        preconditions = []
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = false
        lock = None
    }
    let event2 = {
        id = "http://localhost:8010/events/2"
        label = "dependent event"
        executed = false
        included = true
        pending = false
        conditions = []
        responses = []
        exclusions = []
        inclusions = []
        preconditions = []
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = true
        lock = None
    }
    let client = new WebClient ()
    let host = new Host (8010, host0Url, alwaysLogSuccess, alwaysTrueAuth, alwaysSubscribeSuccessfully)
    let listen = async {
        try
            host.Listen
        with
            _ -> ()
         }
    Async.Start listen
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event1)) |> ignore
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event2)) |> ignore
    let putDto = {
        username = "system"
        value = true
        token = "system-token"
        }
    //act
    let json = JsonConvert.SerializeObject putDto
    try
        client.UploadString ((sprintf "%s/%s" event1.id "executed"), "PUT", json) |> ignore
        let resultingEvent = JsonConvert.DeserializeObject<Event> (client.DownloadString event1.id)
        Assert.True resultingEvent.executed
        Assert.True resultingEvent.lock.IsNone
        let resultingDependentEvent = JsonConvert.DeserializeObject<Event> (client.DownloadString event2.id)
        Assert.True resultingDependentEvent.lock.IsNone
        Assert.False resultingDependentEvent.included
        host.Stop ()
    with
        | :? WebException as webe ->
            host.Stop ()
            let resp = webe.Response :?> HttpWebResponse
            printfn "%A" resp.StatusCode
            printfn "%A" resp.StatusDescription
            Assert.Fail "Could not execute"
        | e ->
            host.Stop ()
            Assert.Fail "Could not execute"

[<Test>]
let ``can execute an event which must lock another event which must lock another event``() =
    let event1 = {
        id = "http://localhost:8010/events/1"
        label = "main event"
        executed = false
        included = true
        pending = false
        conditions = []
        responses = []
        exclusions = ["http://localhost:8010/events/2"]
        inclusions = []
        preconditions = []
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = false
        lock = None
    }
    let event2 = {
        id = "http://localhost:8010/events/2"
        label = "dependent event"
        executed = false
        included = true
        pending = false
        conditions = ["http://localhost:8010/events/3"]
        responses = []
        exclusions = []
        inclusions = []
        preconditions = []
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = true
        lock = None
    }
    let event3 = {
        id = "http://localhost:8010/events/3"
        label = "dependent event"
        executed = false
        included = true
        pending = false
        conditions = []
        responses = []
        exclusions = []
        inclusions = []
        preconditions = ["http://localhost:8010/events/2"]
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = true
        lock = None
    }
    let client = new WebClient ()
    let host = new Host (8010, host0Url, alwaysLogSuccess, alwaysTrueAuth, alwaysSubscribeSuccessfully)
    let listen = async {
        try
            host.Listen
        with
            _ -> ()
         }
    Async.Start listen
    printfn "Sleeperino"
    System.Threading.Thread.Sleep(1000)
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event1)) |> ignore
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event2)) |> ignore
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event3)) |> ignore
    let putDto = {
        username = "system"
        value = true
        token = "system-token"
        }
    //act
    let json = JsonConvert.SerializeObject putDto
    try
        client.UploadString ((sprintf "%s/%s" event1.id "executed"), "PUT", json) |> ignore
        let resultingEvent = JsonConvert.DeserializeObject<Event> (client.DownloadString event1.id)
        Assert.True resultingEvent.executed
        Assert.True resultingEvent.lock.IsNone
        let resultingDependentEvent = JsonConvert.DeserializeObject<Event> (client.DownloadString event2.id)
        Assert.True resultingDependentEvent.lock.IsNone
        Assert.False resultingDependentEvent.included
        let resultingDependentDependentEvent = JsonConvert.DeserializeObject<Event> (client.DownloadString event3.id)
        Assert.True resultingDependentDependentEvent.lock.IsNone
        Assert.True resultingDependentDependentEvent.included
        Assert.True resultingDependentDependentEvent.executable
        Assert.AreEqual ([], resultingDependentDependentEvent.preconditions)
        host.Stop ()
    with
        | :? WebException as webe ->
            host.Stop ()
            let resp = webe.Response :?> HttpWebResponse
            printfn "%A" resp.StatusCode
            printfn "%A" resp.StatusDescription
            Assert.Fail "Could not execute"
        | e ->
            host.Stop ()
            Assert.Fail <| string e

[<Test>]
let ``cannot execute an event which must lock another event which must lock another already locked event``() =
    let event1 = {
        id = "http://localhost:8010/events/1"
        label = "main event"
        executed = false
        included = true
        pending = false
        conditions = []
        responses = []
        exclusions = ["http://localhost:8010/events/2"]
        inclusions = []
        preconditions = []
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = false
        lock = None
    }
    let event2 = {
        id = "http://localhost:8010/events/2"
        label = "dependent event"
        executed = false
        included = true
        pending = false
        conditions = []
        responses = []
        exclusions = []
        inclusions = ["http://localhost:8010/events/3"]
        preconditions = []
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = true
        lock = None
    }
    let event3 = {
        id = "http://localhost:8010/events/3"
        label = "dependent event"
        executed = false
        included = true
        pending = false
        conditions = []
        responses = []
        exclusions = []
        inclusions = []
        preconditions = []
        resources = []
        created = 0
        modified = 0
        roles = []
        executable = true
        lock = Some "Santa Claus"
    }
    let client = new WebClient ()
    let host = new Host (8010, host0Url, alwaysLogSuccess, alwaysTrueAuth, alwaysSubscribeSuccessfully)
    let listen = async {
        try
            host.Listen
        with
            _ -> ()
         }
    Async.Start listen
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event1)) |> ignore
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event2)) |> ignore
    client.UploadString ("http://localhost:8010", "POST", (JsonConvert.SerializeObject event3)) |> ignore
    let putDto = {
        username = "system"
        value = true
        token = "system-token"
        }
    //act
    let json = JsonConvert.SerializeObject putDto
    try
        client.UploadString ((sprintf "%s/%s" event1.id "executed"), "PUT", json) |> ignore
        let resultingEvent = JsonConvert.DeserializeObject<Event> (client.DownloadString event1.id)
        Assert.True resultingEvent.executed
        Assert.True resultingEvent.lock.IsNone
        let resultingDependentEvent = JsonConvert.DeserializeObject<Event> (client.DownloadString event2.id)
        Assert.True resultingDependentEvent.lock.IsNone
        Assert.False resultingDependentEvent.included
        let resultingDependentDependentEvent = JsonConvert.DeserializeObject<Event> (client.DownloadString event3.id)
        Assert.True resultingDependentDependentEvent.lock.IsNone
        host.Stop ()
        Assert.Fail "Should not have received a 200 okay"
    with
        | :? WebException as webe ->
            host.Stop ()
            let resp = webe.Response :?> HttpWebResponse
            printf "%A" resp.StatusDescription
            Assert.AreEqual (503, int resp.StatusCode)
        | e ->
            host.Stop ()
            Assert.Fail <| string e
